<?php
Auth::routes();
Route::group(['prefix'  =>  'admin'], function () {

    Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Admin\LoginController@login')->name('admin.login.post');
    Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout');

    Route::group(['middleware' => ['auth:admin']], function () {

        // Route::get('/', function () {
        //     return view('admin.dashboard.index');
        // })->name('admin.dashboard');

        Route::get('/', 'Admin\DashbordController@index')->name('admin.dashboard');


    // ?strat color option
    Route::prefix('color')->group(function () {
        Route::get('/', 'Admin\ColorController@index')->name('admin.color.index');
        Route::get('/create', 'Admin\ColorController@create')->name('admin.color.create');
        Route::post('/', 'Admin\ColorController@store')->name('admin.color.store');
        Route::get('/{color}/Edit', 'Admin\ColorController@edit')->name('admin.color.edit');
        Route::post('/{color}/Update', 'Admin\ColorController@update')->name('admin.color.update');
        Route::get('/{color}/Delete', 'Admin\ColorController@destroy')->name('admin.color.destroy');
    });
    // ?end color option

        // ?strat size option
        Route::prefix('size')->group(function () {
            Route::get('/', 'Admin\SizeController@index')->name('admin.size.index');
            Route::get('/create', 'Admin\SizeController@create')->name('admin.size.create');
            Route::post('/', 'Admin\SizeController@store')->name('admin.size.store');
            Route::get('/{size}/Edit', 'Admin\SizeController@edit')->name('admin.size.edit');
            Route::post('/{size}/Update', 'Admin\SizeController@update')->name('admin.size.update');
            Route::get('/{size}/Delete', 'Admin\SizeController@destroy')->name('admin.size.destroy');
        });
        // ?end size option

         // ?strat blog option
    Route::prefix('blog')->group(function () {
        Route::get('/', 'Admin\BlogController@index')->name('admin.blog.index');
        Route::get('/create', 'Admin\BlogController@create')->name('admin.blog.create');
        Route::post('/', 'Admin\BlogController@store')->name('admin.blog.store');
        Route::get('/{blog}/Edit', 'Admin\BlogController@edit')->name('admin.blog.edit');
        Route::post('/{blog}/Update', 'Admin\BlogController@update')->name('admin.blog.update');
        Route::get('/{blog}/Delete', 'Admin\BlogController@destroy')->name('admin.blog.destroy');
    });
    // ?end blog option

        Route::prefix('slider')->group(function () {
            Route::get('/', 'Admin\SliderController@index')->name('admin.slider.index');
            Route::get('/create', 'Admin\SliderController@create')->name('admin.slider.create');
            Route::post('/', 'Admin\SliderController@store')->name('admin.slider.store');
            Route::get('/{color}/Edit', 'Admin\SliderController@edit')->name('admin.slider.edit');
            Route::post('/{color}/Update', 'Admin\SliderController@update')->name('admin.slider.update');
            Route::get('/{color}/Delete', 'Admin\SliderController@destroy')->name('admin.slider.destroy');
        });

        // ?end color option
        Route::prefix('gallery')->group(function () {
            Route::get('/', 'Admin\GalleryController@index')->name('admin.gallery.index');
            Route::get('/create', 'Admin\GalleryController@create')->name('admin.gallery.create');
            Route::post('/', 'Admin\GalleryController@store')->name('admin.gallery.store');
            Route::get('/{gallery}/Edit', 'Admin\GalleryController@edit')->name('admin.gallery.edit');
            Route::post('/{gallery}/Update', 'Admin\GalleryController@update')->name('admin.gallery.update');
            Route::get('/{gallery}/Delete', 'Admin\GalleryController@destroy')->name('admin.gallery.destroy');
        });

        // ?end category option
        Route::prefix('category')->group(function () {
            Route::get('/', 'Admin\CategoryController@index')->name('admin.category.index');
            Route::get('/create', 'Admin\CategoryController@create')->name('admin.category.create');
            Route::post('/', 'Admin\CategoryController@store')->name('admin.category.store');
            Route::get('/{category}/Edit', 'Admin\CategoryController@edit')->name('admin.category.edit');
            Route::post('/{category}/Update', 'Admin\CategoryController@update')->name('admin.category.update');
            Route::get('/{category}/Delete', 'Admin\CategoryController@destroy')->name('admin.category.destroy');
        });

        // ?end brand option
        Route::prefix('brand')->group(function () {
            Route::get('/', 'Admin\BrandController@index')->name('admin.brand.index');
            Route::get('/create', 'Admin\BrandController@create')->name('admin.brand.create');
            Route::post('/', 'Admin\BrandController@store')->name('admin.brand.store');
            Route::get('/{brand}/Edit', 'Admin\BrandController@edit')->name('admin.brand.edit');
            Route::post('/{brand}/Update', 'Admin\BrandController@update')->name('admin.brand.update');
            Route::get('/{brand}/Delete', 'Admin\BrandController@destroy')->name('admin.brand.destroy');
        });

        // ?end product option
        Route::prefix('product')->group(function () {
            Route::get('/', 'Admin\ProductController@index')->name('admin.product.index');
            Route::get('/create', 'Admin\ProductController@create')->name('admin.product.create');
            Route::post('/', 'Admin\ProductController@store')->name('admin.product.store');
            Route::get('/{product}/Edit', 'Admin\ProductController@edit')->name('admin.product.edit');
            Route::post('/{product}/Update', 'Admin\ProductController@update')->name('admin.product.update');
            Route::get('/{product}/Delete', 'Admin\ProductController@destroy')->name('admin.product.destroy');


        });

        // ?end product option
        Route::prefix('customer')->group(function () {
            Route::get('/', 'Admin\CustomerController@index')->name('admin.customer.index');
            Route::get('/create', 'Admin\CustomerController@create')->name('admin.customer.create');
            Route::post('/', 'Admin\CustomerController@store')->name('admin.customer.store');
            Route::get('/{customer}/Edit', 'Admin\CustomerController@edit')->name('admin.customer.edit');
            Route::post('/{customer}/Update', 'Admin\CustomerController@update')->name('admin.customer.update');
            Route::get('/{customer}/Delete', 'Admin\CustomerController@destroy')->name('admin.customer.destroy');
        });

        // ?end product show option
        Route::prefix('product_show')->group(function () {
            Route::get('/', 'Admin\ProductShowController@index')->name('admin.product_show.index');
            Route::get('/create', 'Admin\ProductShowController@create')->name('admin.product_show.create');
            Route::post('/', 'Admin\ProductShowController@store')->name('admin.product_show.store');
            Route::get('/{product_show}/Edit', 'Admin\ProductShowController@edit')->name('admin.product_show.edit');
            Route::post('/{product_show}/Update', 'Admin\ProductShowController@update')->name('admin.product_show.update');
            Route::get('/{product_show}/Delete', 'Admin\ProductShowController@destroy')->name('admin.product_show.destroy');
        });

        // ?end product show option
        Route::prefix('coupon')->group(function () {
            Route::get('/', 'Admin\CouponsController@index')->name('admin.coupon.index');
            Route::get('/create', 'Admin\CouponsController@create')->name('admin.coupon.create');
            Route::post('/', 'Admin\CouponsController@store')->name('admin.coupon.store');
            Route::get('/{coupon}/Edit', 'Admin\CouponsController@edit')->name('admin.coupon.edit');
            Route::post('/{coupon}/Update', 'Admin\CouponsController@update')->name('admin.coupon.update');
            Route::get('/{coupon}/Delete', 'Admin\CouponsController@destroy')->name('admin.coupon.destroy');
        });

        // ?end product option
        Route::prefix('order')->group(function () {
            Route::get('/', 'Admin\OrderController@index')->name('admin.order.index');
            Route::get('/create', 'Admin\OrderController@create')->name('admin.order.create');
            Route::post('/', 'Admin\OrderController@store')->name('admin.order.store');
            Route::get('/{order}/Edit', 'Admin\OrderController@edit')->name('admin.order.edit');
            Route::post('/{order}/Update', 'Admin\OrderController@update')->name('admin.order.update');
            Route::get('/{order}/Delete', 'Admin\OrderController@destroy')->name('admin.order.destroy');


            Route::get('/{id}/invoice', 'Admin\OrderController@invoice')->name('admin.order.invoice');
            Route::get('/{id}/order-details', 'Admin\OrderController@order_details')->name('admin.order.order_details');

        });
        Route::prefix('subscribers')->group(function () {
            Route::get('/', 'Admin\SubscribeController@index')->name('admin.subscribers.index');
        });

    });
});
