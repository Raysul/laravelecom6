<?php
require 'admin.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', function () {
//     return view('admin.master');
// });

    // ?end product option
Route::prefix('cart')->group(function () {
    Route::get('/', 'Frontend\CartController@index')->name('cart.index');
    Route::get('/create', 'Frontend\CartController@create')->name('cart.create');
    Route::post('/', 'Frontend\CartController@store')->name('cart.store');
    Route::get('/{cart}/Edit', 'Frontend\CartController@edit')->name('cart.edit');
    Route::post('/{cart}/Update', 'Frontend\CartController@update')->name('cart.update');
    Route::get('/{cart}/Delete', 'Frontend\CartController@destroy')->name('cart.destroy');
});

    // ?end product option
Route::prefix('checkout')->group(function () {
    Route::get('/', 'Frontend\CheckoutController@index')->name('checkout.index');
    Route::get('/create', 'Frontend\CheckoutController@create')->name('checkout.create');
    Route::post('/', 'Frontend\CheckoutController@store')->name('checkout.store');
    Route::get('/{checkout}/Edit', 'Frontend\CheckoutController@edit')->name('checkout.edit');
    Route::post('/{checkout}/Update', 'Frontend\CheckoutController@update')->name('checkout.update');
    Route::get('/{checkout}/Delete', 'Frontend\CheckoutController@destroy')->name('checkout.destroy');
});


    // ?end product option
Route::prefix('wishlist')->group(function () {
    Route::get('/', 'Frontend\WishlistController@index')->name('wishlist.index');
    Route::get('/create', 'Frontend\WishlistController@create')->name('wishlist.create');
    Route::post('/', 'Frontend\WishlistController@store')->name('wishlist.store');
    Route::get('/{wishlist}/Edit', 'Frontend\WishlistController@edit')->name('wishlist.edit');
    Route::post('/{wishlist}/Update', 'Frontend\WishlistController@update')->name('wishlist.update');
    Route::get('/{wishlist}/Delete', 'Frontend\WishlistController@destroy')->name('wishlist.destroy');
});

Route::prefix('user')->group(function () {
    Route::get('/', 'Frontend\UserController@index')->name('user.index');
    Route::get('/create', 'Frontend\UserController@create')->name('user.create');
    Route::post('/', 'Frontend\UserController@store')->name('user.store');
    Route::get('/myprofile', 'Frontend\UserController@myprofile')->name('user.myprofile');
    Route::get('/edit', 'Frontend\UserController@edit')->name('user.edit');
    Route::post('/update', 'Frontend\UserController@update')->name('user.update');
    Route::get('/{user}/Delete', 'Frontend\UserController@destroy')->name('user.destroy');
});
//FOR UI DESIGN TESTING ROUTER
// Route::get('/profile','Frontend\UserController@profile')->name('user.profile');
// Route::get('/profile/edit','Frontend\UserController@editprofile')->name('user.edit');
// Route::get('/profile/myprofile','Frontend\UserController@myprofile')->name('user.myprofile');



// Route::get('/home', 'HomeController@index')->name('home');


Route::get('/', 'Frontend\FrontendController@index')->name('home');
Route::get('{slug}/category', 'Frontend\FrontendController@category')->name('category');
Route::get('{slug}/brand', 'Frontend\FrontendController@brand')->name('brand');
Route::post('subscribe', 'Frontend\FrontendController@subscribe')->name('subscribe');

Route::prefix('product')->group(function () {
    Route::get('/{product}/details', 'Frontend\FrontendController@details')->name('product.details');
});


Route::post('/fetch-company/{companyId}', function($companyId){
    return "jjj";
});

View::composer(['*'], function($view){
    $view->with('categoriess',\App\Models\Category::where('is_active', 1)->get());
    $view->with('sub_categoriess',\App\Models\Category::whereNotNull('parent_id')->where('is_active', 0)->get());
    $view->with('chsil_categoriess',\App\Models\Category::whereNotNull('child_id')->where('is_active', 0)->get());
    $view->with('brands',\App\Models\Brand::all());

});
