<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullbell();
            $table->string('type')->nullbell();
            $table->string('price')->nullbell();
            $table->string('times')->nullbell();
            $table->string('used')->nullbell();
            $table->string('status')->nullbell();
            $table->string('start_date')->nullbell();
            $table->string('end_date')->nullbell();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
