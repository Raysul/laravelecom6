<?php

use PhpParser\Node\Expr\Variable;

define('PANDING', 0);
define('SHIPPED', 1);
define('COMPLETED', 2);

function product_status(){
    $arr = ['Pending','Shipped','Completed','Canceled'];

    foreach($arr as $value){ ?>
         <option value="<?php echo $value ?>"><?php echo $value ?></option>
   <?php }

}

function is_active($is_active){
    if($is_active == 1){
         return 'checked';
    }else{
        return '';
    }
}


function smsSender($phone, $text)
{
    if (is_array($phone)) {
        $phone = implode(',', $phone);
    }
    $fields = array(
        'userId'                        => urlencode('londonexpress'),
        'password'                      => urlencode('London123'),
        'smsText'                       => urlencode($text),
        'commaSeperatedReceiverNumbers' => $phone
    );
    $fields_string = array();
    foreach ($fields as $key => $value) {
        $fields_string[] = $key . '=' . $value;
    }
    $ch = curl_init();
    $url = 'http://powersms.banglaphone.net.bd/httpapi/sendsms/?' . implode('&', $fields_string);
    //$url = 'https://suresmsbd.firebaseapp.com/newSms/?mobile='.$phone.'&text='.urlencode($text).'&username=thebook';
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    curl_close($ch);
}



function bn2enNumber($number)
{
    $search_array = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $en_number = str_replace($search_array, $replace_array, $number);

    return $en_number;
}

function imploadValue($types)
{
    $strTypes = implode(",", $types);
    return $strTypes;
}

function explodeValue($types)
{
    $strTypes = explode(",", $types);
    return $strTypes;
}

function random_code()
{

    return rand(1111, 9999);
}

function remove_special_char($text)
{

    $t = $text;

    $specChars = array(
        ' ' => '-',    '!' => '',    '"' => '',
        '#' => '',    '$' => '',    '%' => '',
        '&amp;' => '',    '\'' => '',   '(' => '',
        ')' => '',    '*' => '',    '+' => '',
        ',' => '',    '₹' => '',    '.' => '',
        '/-' => '',    ':' => '',    ';' => '',
        '<' => '',    '=' => '',    '>' => '',
        '?' => '',    '@' => '',    '[' => '',
        '\\' => '',   ']' => '',    '^' => '',
        '_' => '',    '`' => '',    '{' => '',
        '|' => '',    '}' => '',    '~' => '',
        '-----' => '-',    '----' => '-',    '---' => '-',
        '/' => '',    '--' => '-',   '/_' => '-',

    );

    foreach ($specChars as $k => $v) {
        $t = str_replace($k, $v, $t);
    }

    return $t;
}

function active($path)
{
    $data = Request::is($path . '*');
    return Request::is($path . '*') ? ' class=active' :  '';
}

function setActive(string $path, string $class_name = 'class=active')
{
    $request_path = implode('/', Request::segments());
    return $request_path === $path ? $class_name : "class=active";
}

function required($ster = '*')
{
    return '<span style=color:red>' . $ster . '</span>';
}

function is_set_value($data){
    if($data){
        return $data;
    }else{
        return "";
    }
}

function make_date($data){
    return $data->format('d/m/Y:a');
}
function test()
{
    return "hello";
}
