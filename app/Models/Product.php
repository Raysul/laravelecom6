<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    function size()
    {
        return $this->belongsTo('App\Models\Size');
    }
    function color()
    {
        return $this->belongsTo('App\Models\Color');
    }
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }
    function product_show_position(){
        return $this->belongsTo('App\Models\Product_show', 'product_show');
    }
    public function best_sale_of_the_week()
    {
        return Product::where('product_show', 1)->latest()->limit(3)->get();
    }
    public function customer_choice()
    {
        return Product::where('product_show', 5)->latest()->limit(3)->get();
    }
    public function top_sellers_in()
    {
        return Product::where('product_show', 2)->latest()->limit(5)->get();
    }
    public function woman_styles()
    {
        return Product::where('product_show', 3)->latest()->limit(8)->get();
    }
    function woman_clothings(){
        return Product::where('category_id', 13)->where('product_show', 3)->latest()->limit(8)->get();
    }
    function woman_footwears(){
        return Product::where('category_id', 14)->where('product_show', 3)->latest()->limit(8)->get();
    }
    function woman_jewellerys(){
        return Product::where('category_id', 15)->where('product_show', 3)->latest()->limit(8)->get();
    }
    public function man_style()
    {
        return Product::where('product_show', 4)->latest()->limit(8)->get();
    }
    function man_clothings(){
        return Product::where('category_id', 13)->where('product_show', 4)->latest()->limit(8)->get();
    }
    function man_footwears(){
        return Product::where('category_id', 14)->where('product_show', 4)->latest()->limit(8)->get();
    }
    function man_Bags(){
        return Product::where('category_id', 16)->where('product_show', 4)->latest()->limit(8)->get();
    }
    function top_sallers_mobailes(){
        return Product::where('category_id', 6)->where('product_show', 2)->latest()->limit(3)->get();
    }
    function top_sallers_computers(){
        return Product::where('category_id', 9)->where('product_show', 2)->latest()->limit(3)->get();
    }
    function top_sallers_clothess(){
        return Product::where('category_id', 10)->where('product_show', 2)->latest()->limit(3)->get();
    }
    function top_sallers_accessoriess(){
        return Product::where('category_id', 11)->where('product_show', 2)->latest()->limit(3)->get();
    }


}
