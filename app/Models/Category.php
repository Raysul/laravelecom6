<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function scope_active($query)
    {
        return $query->where('is_active', 1);
    }

    public function product(){
        return $this->hasMany('App\Models\Product');
    }
}
