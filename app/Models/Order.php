<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    function user(){
        return $this->belongsTo('App\Models\User');
    }

    function carts(){
        return $this->hasMany('App\Models\Cart');
    }

    function payment(){
        return $this->belongsTo('App\Models\payment');
    }
 
}
