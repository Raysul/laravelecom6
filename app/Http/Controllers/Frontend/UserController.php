<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use App\Models\User;
use App\Models\Customer;
use App\Models\Divisions;
use App\Models\Districts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Image;
use File;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('frontend.users.dashboard',compact('user'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function myprofile(){
        $user_id = Auth::id();
        $user = User::with('customer')->where('id', $user_id)->first();
        return view('frontend.users.index',compact('user'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user_id = Auth::id();
        $divisions = Divisions::all();
        $districts = Districts::all();
        $user = User::with('customer')->where('id', $user_id)->first();
        // return $user;
        return view('frontend.users.edit',compact('divisions', 'districts', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $customer = Customer::where('user_id', $user_id)->first();
        $check_email = User::where('email', $request->email)->where('id', '!=' ,$user_id)->first();
        $check_phone = Customer::where('phone', $request->phone)->where('user_id', '!=' ,$user_id)->first();
        if($check_email){
            return redirect()->route('user.edit')->with("error", "Email Already Exist");
        }elseif($check_phone){
            return redirect()->route('user.edit')->with("error", "Phone Number  Already Exist");
        }else{
             $user->name = $request->name;
             $user->email = $request->email;
            if($customer){
                $customer->user_id = $user_id;
                $customer->phone = $request->phone;
                $customer->address = $request->address;
                $customer->division_id = $request->division_id;
                $customer->district_id = $request->district_id;
                if ($request->image) {
                    if (File::exists('uploads/profile/' . $customer->image)) {
                        File::delete('uploads/profile/' . $customer->image);
                    }
                    $image = $request->file('image');
                    $img = time() . '.' . $image->getClientOriginalExtension();
                    $location = public_path('uploads/profile/' . $img);
                    Image::make($image)->resize(100, 100)->save($location);
                    $customer->image = $img;
                }
                $customer->update();
            }else{
                $unique_phone = Customer::where('phone', $request->phone)->first();
                if($unique_phone){
                    return redirect()->route('user.edit')->with("error", "Phone Number  Already Exist");
                }
                $customer = new Customer();
                $customer->user_id = $user_id;
                $customer->phone = $request->phone;
                $customer->address = $request->address;
                $customer->division_id = $request->division_id;
                $customer->district_id = $request->district_id;
                if ($request->image) {
                    $image = $request->file('image');
                    $img = time() . '.' . $image->getClientOriginalExtension();
                    $location = public_path('uploads/profile/' . $img);
                    Image::make($image)->resize(100, 100)->save($location);
                    $customer->image = $img;
                $customer->save();
            }
            $user->update();
        }
        return redirect()->route('user.myprofile')->with("msg", "Profile Update successfully");

    }
}
    //Some change

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
