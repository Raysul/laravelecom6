<?php

namespace App\Http\Controllers\Frontend;


use App\Models\Blog;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;

class FrontendController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = new Product();
        $data['sliders'] = Slider::all();
        $data['blogs'] = Blog::all();
        $data['best_sale_of_the_weeks'] = $product->best_sale_of_the_week();
        $data['customer_choices'] = $product->customer_choice();
        $data['top_sellers_ins'] = $product->top_sellers_in();
        $data['woman_styles'] = $product->woman_styles();
        $data['woman_clothings'] = $product->woman_clothings();
        $data['woman_footwears'] = $product->woman_footwears();
        $data['woman_jewellerys'] = $product->woman_jewellerys();
        $data['man_styles'] = $product->man_style();
        $data['man_clothings'] = $product->man_clothings();
        $data['man_footwears'] = $product->man_footwears();
        $data['man_Bags'] = $product->man_Bags();
        $data['top_sallers_mobailes'] = $product->top_sallers_mobailes();
        $data['top_sallers_computers'] = $product->top_sallers_computers();
        $data['top_sallers_clothess'] = $product->top_sallers_clothess();
        $data['top_sallers_accessoriess'] = $product->top_sallers_accessoriess();

        return view('frontend.index', compact('data'));
    }

    public function category($slug){
        $data['category'] = Category::where('slug', $slug)->first();
        if($data['category']){
            $data['products'] = Product::where('category_id', $data['category']->id)->paginate(15);
            return view('frontend.category', compact('data'));
        }else{
            return view('frontend.error_404');
        }
    }

    public function brand($slug){
        $data['brand'] = Brand::where('slug', $slug)->first();
        if($data['brand']){
            $data['products'] = Product::where('brand_id', $data['brand']->id)->paginate(15);
            return view('frontend.category', compact('data'));
        }else{
            return view('frontend.error_404');
        }
    }

    /**
     * param product_id
     * return a view
     */
    public function details(Request $request, $product){
        $product = new Product();
        $data['product'] = Product::find($product);
        $data['top_sallers_mobailes'] = $product->top_sallers_mobailes();
        return view('frontend.details',compact('data'));
    }

    public function subscribe(Request $request){
        $subscribe = new Subscribe();
        $subscribe->email = $request->email;
        $subscribe->save();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
