<?php 
namespace App\Http\Controllers\API; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Wishlist;
use Auth;

class WishlistsController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $carts = Cart::totalCarts();
        
        return view('frontend.cart', compact('carts'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'product_id' => 'required'
            ],
            [
                'product_id.required' => 'Please Choose Your Product'
            ]
        ); 
            $wishlist = new Wishlist();
            if (Auth::check()) {
                $wishlist->user_id = Auth::id();
            }
            $wishlist->ip_address = request()->ip();
            $wishlist->product_id = $request->product_id;
            $wishlist->save(); 
        return json_encode([ 'status' => 'success', 'Message' => 'Wishlist Added']);
    }

    public function update(Request $request){
     
    }

    public function destroy(Request $request)
    {
       
    }
}
