<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Cart;
use Auth;

class CartsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {

        $carts = Cart::totalCarts();
        return view('frontend.cart', compact('carts'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'product_id' => 'required'
            ],
            [
                'product_id.required' => 'Please Choose Your Product'
            ]
        );

        if (Auth::check()) {
            $cart = Cart::where('user_id', Auth::id())->where('product_id', $request->product_id)->where('order_id', NULL)->first();
        } else {
            $cart = Cart::where('ip_address', request()->ip())->where('product_id', $request->product_id)->where('order_id', NULL)->first();
        }


        if ($cart) {
            $cart->increment('product_quantity');
        } else {
            $cart = new Cart();
            if (Auth::check()) {
                $cart->user_id = Auth::id();
            }
            $cart->ip_address = request()->ip();
            $cart->product_id = $request->product_id;
            $cart->save();
        }
        return json_encode([ 'status' => 'success', 'Message' => 'Item Added to the Cart', 'totalItems' => Cart::totalItems() ]);
    }

    public function update(Request $request){
        $cart = Cart::find($request->cart_id);
        $cart->product_quantity= $request->product_quantity;
        $cart->update();
        return json_encode([ 'status' => 'success', 'Message' => 'Item Added to the Cart', 'totalItems' => Cart::totalItems() ]);
    }

    public function destroy(Request $request)
    {
        $cart = Cart::find($request->cart_id);
        $cart->delete();
        return json_encode([ 'status' => 'success', 'Message' => 'Item Added to the Cart', 'cart_id' => $cart->id]);
    }

}
