<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Category;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $category = new Category();
        $request->validate([
            'name' => 'required',
        ]);
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->parent_id = $request->parent_id;
        $category->child_id = $request->child_id;
        $category->desc = $request->desc;
        $category->is_active = isset($request->is_active) ? $request->is_active : '0';
        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/categoriesImage/' . $img);
            Image::make($image)->resize(1920, 471)->save($location);
            $category->image = $img;
        }

        $category->save();
        return redirect()->route('admin.category.index')->with("msg", "Category insert successfully");
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        $categories = Category::all();
        $category = Category::find($category);
        return view('admin.category.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        // dd($request->all());
        $category = Category::find($category);
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->parent_id = $request->parent_id;
        $category->child_id = $request->child_id;
        $category->desc = $request->desc;
        $category->is_active = isset($request->is_active) ? $request->is_active : '0';
        if ($request->image) {
            if (File::exists('uploads/categoriesImage/' . $category->image)) {
                File::delete('uploads/categoriesImage/' . $category->image);
            }
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/c/' . $img);
            Image::make($image)->resize(1920, 471)->save($location);
            $category->image = $img;
        }

        $category->update();
        return redirect()->route('admin.category.index')->with("msg", "Catagory Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        // dd($category);
        $category = Category::find($category);
        if (!empty($category)) {
            if (File::exists('uploads/category/' . $category->image)) {
                File::delete('uploads/category/' . $category->image);
            }

            $category->delete();
        }
        return back();
    }
}
