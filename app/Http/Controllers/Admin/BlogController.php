<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use File;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::latest()->get();
        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $Blog = new Blog();
        $request->validate([
            'title' => 'bail|required|max:255',
            'author' => 'bail|required',
            'description' => 'bail|required',
        ]);

        $Blog->title = $request->title;
        $Blog->author = $request->author;
        $Blog->slug = Str::slug($request->title);
        $Blog->description = $request->description;

        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/blogImage/' . $img);
            Image::make($image)->resize(250, 230)->save($location);
            $Blog->image = $img;
        }

        $Blog->save();
        return redirect()->route('admin.blog.create')->with("msg", "Blog insert successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($blog)
    {
        $Blog = Blog::find($blog);
        return view('admin.blog.edit', compact('Blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $blog)
    {
        $Blog = Blog::find($blog);
        $request->validate([
            'title' => 'bail|required|max:255',
            'author' => 'bail|required',
            'description' => 'bail|required',
        ]);

        $Blog->title = $request->title;
        $Blog->author = $request->author;
        $Blog->slug = Str::slug($request->title);
        $Blog->description = $request->description;

        if ($request->image) {
            if (File::exists('uploads/blogImage/' . $Blog->image)) {
                File::delete('uploads/blogImage/' . $Blog->image);
            }
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/blogImage/' . $img);
            Image::make($image)->resize(250, 230)->save($location);
            $Blog->image = $img;
        }

        $Blog->update();
        return back()->with("msg", "Blog insert successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog)
    {
        $Blog = Blog::find($blog);
        if (!empty($Blog)) {
            if (File::exists('uploads/blogImage/' . $Blog->image)) {
                File::delete('uploads/blogImage/' . $Blog->image);
            }
            $Blog->delete();
        }
        return back();
    }
}
