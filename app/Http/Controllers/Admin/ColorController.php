<?php

namespace App\Http\Controllers\Admin;


use App\Models\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::latest()->get();
        return view('admin.color.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // dd($request->all());
        $color = new Color();
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $color->name = $request->name;
        $color->hexadecimal = $request->hexadecimal;

        $color->save();
        return redirect()->route('admin.color.index')->with("msg", "Color insert successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit($color)
    {
        $color = Color::find($color);
        return view('admin.color.edit', compact('Color'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $color)
    {
        // dd($request->all());
        $color = Color::find($color);
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $color->name = $request->name;
        $color->hexadecimal = $request->hexadecimal;

        $color->update();
        return back()->with("msg", "Color update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy($color)
    {
        $color = Color::find($color);
        $color->delete();
        return back();
    }
}
