<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallerys = Gallery::orderBy('id', 'DESC')->get();
        return view('admin.gallery.index', compact('gallerys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $gallery = new Gallery();
        $request->validate([
            'image' => 'bail|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $gallery->title = $request->title;
        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/galleryImage/' . $img);
            Image::make($image)->resize(1920, 471)->save($location);
            $gallery->image = $img;
        }

        $gallery->save();
        return redirect()->route('admin.gallery.create')->with("msg", "Slider insert successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $gallery = Gallery::find($id);
        return view('admin.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $gallery = Gallery::find($id);
        $gallery->title = $request->title;
        if ($request->image) {
             if (File::exists('uploads/galleryImage/' . $gallery->image)) {
                 File::delete('uploads/galleryImage/' . $gallery->image);
             }
             $image = $request->file('image');
             $img = time() . '.' . $image->getClientOriginalExtension();
             $location = public_path('uploads/galleryImage/' . $img);
             Image::make($image)->resize(1920, 471)->save($location);
             $gallery->image = $img;
         }

         $gallery->update();
         return back()->with("msg", "Gallery Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::find($id);
        if (!empty($gallery)) {
            if (File::exists('uploads/galleryImage/' . $gallery->image)) {
                File::delete('uploads/galleryImage/' . $gallery->image);
            }
            $gallery->delete();
        }
        return back();
    }
}
