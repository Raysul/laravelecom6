<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class BrandController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::orderBy('id', 'DESC')->get();
        return view('admin.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->slug = Str::slug($request->name);
        $brand->desc = $request->desc;

        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/brandImage/' . $img);
            Image::make($image)->resize(1920, 471)->save($location);
            $brand->image = $img;
        }

        $brand->save();
        return redirect()->route('admin.brand.index')->with("msg", "Category insert successfully");
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($brand)
    {
        $brand = Brand::find($brand);
        return view('admin.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $brand)
    {

        $brand = brand::find($brand);
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $brand->name = $request->name;
        $brand->slug = Str::slug($request->name);
        $brand->desc = $request->desc;
        if ($request->image) {
            if (File::exists('uploads/brandImage/' . $brand->image)) {
                File::delete('uploads/brandImage/' . $brand->image);
            }
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/brandImage/' . $img);
            Image::make($image)->resize(1920, 471)->save($location);
            $brand->image = $img;
        }

        $brand->update();
        return redirect()->route('admin.brand.index')->with("msg", "Catagory Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($brand)
    {
        $brand = brand::find($brand);
        if (!empty($brand)) {
            if (File::exists('uploads/category/' . $brand->image)) {
                File::delete('uploads/category/' . $brand->image);
            }

            $brand->delete();
        }
        return back();
    }
}
