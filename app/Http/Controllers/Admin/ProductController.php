<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Models\Size;
use App\Models\Color;
use App\Models\Product_show;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::with('category', 'product_show_position', 'brand', 'color', 'size')->orderBy('id', 'DESC')->get();
        return view('admin.product.index', compact('products', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys = Category::all();
        $brands = Brand::all();
        $product_shows = Product_show::all();
        $colors = Color::all();
        $sizes = Size::all();
        return view('admin.product.create', compact('categorys', 'brands', 'product_shows','colors','sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(json_encode($request->color_id));
        $admin_id = Auth::guard('admin')->user()->id;
        $product = new Product();
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'size' => 'required',
            'color' => 'required',
            'image' => 'bail|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $product->name = $request->name;
        $product->slug = Str::slug($request->name);
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->desc = $request->desc;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->offer_price = $request->offer_price;
        $product->product_show = $request->product_show;
        $product->color_id = json_encode($request->color_id);
        $product->size_id = json_encode($request->size_id);
        $product->is_active = isset($request->is_active) ? $request->is_active : '0';
        $product->admin_id = $admin_id;

        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/productImage/' . $img);
            Image::make($image)->resize(800, 600)->save($location);
            // Image::make($image)->save($location);
            $product->image = $img;
        }

        $product->save();
        //  dd($request->all());
        return redirect()->route('admin.product.index')->with("msg", "Product insert successfully");
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categorys = Category::all();
        $product_shows = Product_show::all();
        $colors = Color::all();
        $sizes = Size::all();
        $brands = Brand::all();
        return view('admin.product.edit', compact('product', 'categorys', 'product_shows', 'brands','colors','sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product)
    {
        // dd($request->all());
        $admin_id = Auth::guard('admin')->user()->id;
        $product = Product::find($product);
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $product->name = $request->name;
        $product->slug = Str::slug($request->name);
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->desc = $request->desc;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->offer_price = $request->offer_Price;
        $product->product_show = $request->product_show;
        $product->color_id = json_encode($request->color_id);
        $product->size_id = json_encode($request->size_id);
        $product->is_active = isset($request->is_active) ? $request->is_active : '0';
        $product->admin_id = $admin_id;
     if ($request->image) {
         if (File::exists('uploads/productImage/' . $product->image)) {
             File::delete('uploads/productImage/' . $product->image);
         }
         $image = $request->file('image');
         $img = time() . '.' . $image->getClientOriginalExtension();
         $location = public_path('uploads/productImage/' . $img);
         Image::make($image)->resize(800, 600)->save($location);
         $product->image = $img;
     }

     $product->update();
     return redirect()->route('admin.product.index')->with("msg", "Product update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
        $product = Product::find($product);
        if (!empty($product)) {
            if (File::exists('uploads/productImage/' . $product->image)) {
                File::delete('uploads/productImage/' . $product->image);
            }

            $product->delete();
        }
        return back();
    }


}
