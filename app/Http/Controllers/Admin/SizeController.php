<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = Size::latest()->get();
         return view('admin.size.index', compact('sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.size.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Size = new Size();
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);
        $Size->name = $request->name;
        $Size->save();
        return redirect()->route('admin.size.index')->with("msg", "Size insert successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function show(Size $size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function edit($size)
    {
        $Size = Size::find($size);
        return view('admin.size.edit', compact('Size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $size)
    {
        $Size = Size::find($size);
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $Size->name = $request->name;
        $Size->update();
        return redirect()->route('admin.size.index')->with("msg", "Size Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy($size)
    {
        $Size = Size::find($size);
        $Size->delete();
        return back();
    }
}
