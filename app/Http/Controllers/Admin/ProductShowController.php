<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Product_show;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_show = Product_show::all();
        return view('admin.product_show.index', compact('product_show'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product_show.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_show = new Product_show();
        $request->validate([
            'name' => 'required',
        ]);
        $product_show->name = $request->name;
        $product_show->slug = Str::slug($request->name);
        $product_show->is_active = $request->is_active;


        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/product_showImage/' . $img);
            Image::make($image)->resize(600, 400)->save($location);
            $product_show->image = $img;
        }

        $product_show->save();
        return redirect()->route('admin.product_show.create')->with('msg', 'Product show insert successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product_show  $product_show
     * @return \Illuminate\Http\Response
     */
    public function show(Product_show $product_show)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product_show  $product_show
     * @return \Illuminate\Http\Response
     */
    public function edit($product_show)
    {
        $product_show = Product_show::find($product_show);
        return view('admin.product_show.edit', compact('product_show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product_show  $product_show
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_show)
    {
        $product_show = Product_show::find($product_show);
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);

        $product_show->name = $request->name;
        $product_show->slug = Str::slug($request->name);
        $product_show->is_active = $request->is_active;
        if ($request->image) {
            if (File::exists('uploads/product_showImage/' . $product_show->image)) {
                File::delete('uploads/product_showImage/' . $product_show->image);
            }
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/c/' . $img);
            Image::make($image)->resize(600, 400)->save($location);
            $product_show->image = $img;
        }

        $product_show->update();
        return redirect()->route('admin.product_show.index')->with("msg", "product_show Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product_show  $product_show
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_show)
    {
        $product_show = Product_show::find($product_show);
        if (!empty($product_show)) {
            if (File::exists('uploads/product_showImage/' . $product_show->image)) {
                File::delete('uploads/product_showImage/' . $product_show->image);
            }

            $product_show->delete();
        }
        return back();
    }
}
