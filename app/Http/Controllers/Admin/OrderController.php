<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('user', 'carts')->get();
        // $prices = Cart::with('product')->get();
        // dd($orders->id);
        return view('admin.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }


    public function invoice($id)
    {
        $order = Order::find($id);
        $invoices = Cart::with('product', 'order')->where('order_id',$id)->get();
        $customers = DB::table('users')->where('users.id', $order->user_id)->leftJoin('customers', 'users.id', '=', 'customers.user_id')->first();
        return view('admin.order.invoice', compact('invoices', 'customers'));
    }

    function order_details($id){
        $order = Order::where('id', $id)->with('payment')->first();
        $products = Cart::with('product', 'order')->where('order_id',$id)->get();
        $customers = DB::table('users')->where('users.id', $order->user_id)->leftJoin('customers', 'users.id', '=', 'customers.user_id')->first();
        return view('admin.order.order_details', compact('order','products', 'customers'));
    }
}
