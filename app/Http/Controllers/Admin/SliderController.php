<?php

namespace App\Http\Controllers\Admin;

use File;
use Image;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::orderBy('id', 'DESC')->get();
        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $slider = new Slider();
        $request->validate([
            'image' => 'bail|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $slider->name = $request->name;
        $slider->title = $request->title;
        $slider->price = $request->price;
        $slider->offerPrice = $request->offerPrice;

        if ($request->image) {
            $image = $request->file('image');
            $img = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/sliderImage/' . $img);
            Image::make($image)->resize(840, 500)->save($location);
            $slider->image = $img;
        }

        $slider->save();
        return redirect()->route('admin.slider.index')->with("msg", "Slider insert successfully");
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slider)
    {
        // dd($request->all());
        $slider = Slider::find($slider);
        $request->validate([
         'name' => 'bail|required|max:255',
     ]);

     $slider->name = $request->name;
     $slider->title = $request->title;
     $slider->price = $request->price;
     $slider->offerPrice = $request->offerPrice;

     if ($request->image) {
         if (File::exists('uploads/sliderImage/' . $slider->image)) {
             File::delete('uploads/sliderImage/' . $slider->image);
         }
         $image = $request->file('image');
         $img = time() . '.' . $image->getClientOriginalExtension();
         $location = public_path('uploads/sliderImage/' . $img);
         Image::make($image)->resize(840, 500)->save($location);
         $slider->image = $img;
     }

     $slider->update();
     return back()->with("msg", "Slider Update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider = Slider::find($slider);
        if (!empty($slider)) {
            if (File::exists('uploads/sliderImage/' . $slider->image)) {
                File::delete('uploads/sliderImage/' . $slider->image);
            }

            $slider->delete();
        }
        return back();
    }
}
