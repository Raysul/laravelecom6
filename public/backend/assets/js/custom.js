
// category start
    $("#parent_id").change(function() {
        $('.sub_category').css("display", "none");
        $('#change_name').text("Sub Category");
    });

    $("#child_id").change(function() {
        $('.sub_primary').css("display", "none");
        $('#change_name').text("Child Category");
    });
// category end




function deleteCategory(category)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id = category;
    alert(id);
    $.ajax({
        type: "GET",
        url: "/admin/category/",
        data: {
            category: category,
        },
        success: function (data) {
            if (data.success == 1) {
                $("#product-table_wrappers tbody").html('');
                var sl = 1;
                $.each(data.order, function (i, v) {
                    $("#product-table_wrappers tbody").append('<tr><td>' + sl + '</td><td>' + v.date + '</td><td>' + v.order_no + '</td><td>' + v.customer + '</td><td>' + v.amount + '</td></tr>');
                    sl++;
                });
                $("#product-table_wrappers tbody").append('<tr><td colspan="4">Total</td><td>' + data.total + '</td></tr>');
            }
        }
    });
}
