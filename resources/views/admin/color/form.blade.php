    <div class="form-group row">
    <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <canvas id="picker"></canvas><br>
        </div>
    </div>


    <div class="form-group row">
    <label class="col-sm-2 col-form-label">hexadecimal</label>
        <div class="col-sm-10">
            <input id="color" class="form-control" name="hexadecimal" value="{{ old('hexadecimal', $Color->hexadecimal ?? '') }}">
            <script>
                new KellyColorPicker({place : 'picker', input : 'color', size : 150});
            </script>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="name" value="{{ old('name', $Color->name ?? '') }}">
        </div>
    </div>




