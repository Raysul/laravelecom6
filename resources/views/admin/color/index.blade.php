@extends('admin.master')

@section('title')
    Add Color list
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <h5 class="font-strong mb-4">Color LIST</h5>
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Type:</label>
                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                        <option value="">All</option>
                        <optgroup label="Electronics">
                            {{--  @foreach ($CategoryTypes as $CategoryType)
                            <option>{{  $CategoryType->name }}</option>
                            @endforeach  --}}
                        </optgroup>
                    </select>
                </div>
                <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    <a class="btn btn-rounded btn-primary btn-air" href="{{ route('admin.color.create') }}">Add Color</a>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="products-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Hexadecimal</th>
                            <th style="text-align:center; width: 110px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($colors as $color)
                        <tr>
                            <td>1</td>
                            <td>{{ $color->name }}</td>
                            <td>
                                <button class="btn btn-primary btn-square" style="
                                width: 230px;
                                padding: 15px;
                                background: {{ $color->hexadecimal }};"></button>
                            </td>

                            <td class="text-center">
                                <a href="{{ route('admin.color.edit', $color->id) }}">
                                    <button class="btn btn-soc-facebook btn-icon-only"><i class="la la-pencil"></i></button>
                                </a>
                                <a href="{{ route('admin.color.destroy', $color->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">
                                    <button class="btn btn-soc-youtube btn-icon-only"><i class="la la-trash"></i></button>
                                </a>
                             </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
