@extends('admin.master')

@section('title')
    Add Blog list
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <h5 class="font-strong mb-4">Blog LIST</h5>
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Type:</label>
                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                        <option value="">All</option>
                        <optgroup label="Electronics">
                            {{-- @foreach ($CategoryTypes as $CategoryType)
                            <option>{{  $CategoryType->name }}</option>
                            @endforeach --}}
                        </optgroup>
                    </select>
                </div>
                <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    <a class="btn btn-rounded btn-primary btn-air" href="{{ route('admin.brand.create') }}">Add Blog</a>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="products-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Autor</th>
                            <th style="text-align:center; width: 60px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($blogs as $blog)
                        <tr>
                            <td>1</td>
                            <td>
                                @if(!empty($blog->image))
                                    <img class="mr-3" src="{{ asset('uploads/blogImage/'.$blog->image) }}" alt="image" width="60" />
                                @else
                                    <img class="mr-3" src="{{ asset('uploads/blogImage/notFound.jpg') }}" alt="image" width="60" />
                                @endif
                            </td>
                           <td>{{ $blog->title }}</td>
                           <td>{{ $blog->author }}</td>

                             <td class="text-center">
                                  <a href="{{ route('admin.blog.edit', $blog->id) }}">
                                            <button class="btn btn-soc-facebook btn-icon-only"><i class="la la-pencil"></i></button>
                                       </a>
                                        <a href="{{ route('admin.blog.destroy', $blog->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">
                                            <button class="btn btn-soc-youtube btn-icon-only"><i class="la la-trash"></i></button>
                                        </a>
                             </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
