<div class="form-group  row">
        <label class="col-sm-2 col-form-label">Image</label>
        <div class="col-sm-10">
            <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
                <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
            </label>
        </div>
        <label class="col-sm-2 col-form-label"></label>
        <label class="col-sm-2 col-form-label">
            <div id="targetLayer">
                @if(!empty($Blog->image))
                    <img src="{{ asset('uploads/blogImage/'.$Blog->image) }}" width="200px" height="200px"/>
                @endif
            </div>
        </label>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="title" value="{{ old('name', $Blog->title ?? '') }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea name="description">{{ old('title', $Blog->description ?? '') }}</textarea>
            <script>
            CKEDITOR.replace('description');
            </script>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Author</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="author" value="{{ old('name', $Blog->author ?? '') }}">
        </div>
    </div>

