@extends('admin.master')

@section('title')
Add new product show
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-heading">
    <h1 class="page-title">Update product show </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">product show </li>
        <li class="breadcrumb-item">Update product show </li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Edit product show  Information</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="ti-angle-down"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"></label>
            </div>

            <form method="POST" action="{{ route('admin.product_show.update', $product_show->id) }}" name="editForm" class="form-horizontal" id="form-sample-1"
                novalidate="novalidate" enctype="multipart/form-data">
                @csrf
               {{--  include form   --}}
               @include('admin.product_show.form')

               <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-primary btn-fix">
                            <span class="btn-icon">Update</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
