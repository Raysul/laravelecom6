<div class="form-group  row">
        <label class="col-sm-2 col-form-label">Image</label>
        <div class="col-sm-10">
            <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
                <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
            </label>
        </div>
        <label class="col-sm-2 col-form-label"></label>
        <label class="col-sm-2 col-form-label">
            <div id="targetLayer">
            @if(!empty($product_show->image))
                <img src="{{ asset('uploads/product_showImage/'.$product_show->image) }}" width="200px" height="200px"/>
            @endif
            </div>
    </label>
</div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name {!! required() !!}</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="name" value="{{ old('name', $product_show->name ?? '') }}">
        </div>
    </div>

    <div class="form-group row">
        <div class="col-2">Active {!! required() !!}</div>
            <div class="col-3">
                <label class="ui-switch">
                    <input type="checkbox" name="is_active" value="{{ old('is_active', $product_show->is_active ?? '') }}" {!! is_active(@$product_show->is_active) !!}>
                    <span></span>
                </label>
            </div>
    </div>




