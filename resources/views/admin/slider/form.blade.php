<div class="form-group  row">
        <label class="col-sm-2 col-form-label">Image {!! required() !!}</label>
        <div class="col-sm-10">
            <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
                <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
            </label>
        </div>
        <label class="col-sm-2 col-form-label"></label>
        <label class="col-sm-2 col-form-label">
            <div id="targetLayer">
            @if(!empty($slider->image))
                <img src="{{ asset('uploads/sliderImage/'.$slider->image) }}" width="200px" height="200px"/>
            @endif
            </div>
    </label>
</div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="name" value="{{ old('name', $slider->name ?? '') }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="title" value="{{ old('title', $slider->title ?? '') }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Price</label>
        <div class="col-sm-10">
            <input class="form-control" type="number" name="price" value="{{ old('price', $slider->price ?? '') }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Offer Price</label>
        <div class="col-sm-10">
            <input class="form-control" type="number" name="offerPrice" value="{{ old('offerPrice', $slider->offerPrice ?? '') }}">
        </div>
    </div>


