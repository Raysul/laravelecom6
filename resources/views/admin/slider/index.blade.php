@extends('admin.master')

@section('title')
Add Product list
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <h5 class="font-strong mb-4">Slider List</h5>
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Type:</label>
                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                        <option value="">All</option>
                        <optgroup label="Electronics">
                            {{-- @foreach ($CategoryTypes as $CategoryType)
                            <option>{{  $CategoryType->name }}</option>
                            @endforeach --}}
                        </optgroup>
                    </select>
                </div>
                <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    <a class="btn btn-rounded btn-primary btn-air" href="{{ route('admin.slider.create') }}">Add slider</a>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="products-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Offer Price</th>
                            <th style="text-align:center; width: 110px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($sliders as $slider)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>
                                @if(!empty($slider->image))
                                <img class="mr-3" src="{{ asset('uploads/sliderImage/'.$slider->image) }}" alt="image" width="60" />
                                @else
                                <img class="mr-3" src="{{ asset('uploads/sliderImage/notFound.jpg') }}" alt="image" width="60" />
                                @endif
                            </td>
                            <td>{{ $slider->name }}</td>
                            <td>{{ $slider->title }}</td>
                            <td>{{ $slider->price }}</td>
                            <td>{{ $slider->offerPrice }}</td>

                            <td class="text-center">
                                <a href="{{ route('admin.slider.edit', $slider->id) }}">
                                    <button class="btn btn-soc-facebook btn-icon-only"><i class="la la-pencil"></i></button>
                                </a>
                                <a href="{{ route('admin.slider.destroy', $slider->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">
                                    <button class="btn btn-soc-youtube btn-icon-only"><i class="la la-trash"></i></button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
