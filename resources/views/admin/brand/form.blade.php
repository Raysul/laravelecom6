<div class="form-group  row">
        <label class="col-sm-2 col-form-label">Image</label>
        <div class="col-sm-10">
            <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
                <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
            </label>
        </div>
        <label class="col-sm-2 col-form-label"></label>
        <label class="col-sm-2 col-form-label">
            <div id="targetLayer">
            @if(!empty($brand->image))
                <img src="{{ asset('uploads/brandImage/'.$brand->image) }}" width="200px" height="200px"/>
            @endif
            </div>
    </label>
</div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Name {!! required() !!}</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="name" value="{{ old('name', $brand->name ?? '') }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea name="desc">{{ old('desc', $brand->desc ?? '') }}</textarea>
            <script>
            CKEDITOR.replace('desc');
            </script>
        </div>
    </div>


