@extends('admin.master')

@section('title')
Subscribe list
@endsection

@section('style')
<link href="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <div class="flexbox mb-5">
                <h5 class="font-strong">Subscribe List</h5>
                <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    {{-- <a class="btn btn-rounded btn-primary btn-air" href="ecommerce_add_product.html">Add Product</a> --}}
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="customers-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Email</th>

                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($subscribes as $subscribe)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>
                                     {{ $subscribe->email }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.js"></script>

    <script>
        $(function() {
            $('#customers-table').DataTable({
                pageLength: 10,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
                columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
            });

            var table = $('#customers-table').DataTable();
            $('#key-search').on('keyup', function() {
                table.search(this.value).draw();
            });
            $('#type-filter').on('change', function() {
                table.column(2).search($(this).val()).draw();
            });
        });
    </script>
@endsection

