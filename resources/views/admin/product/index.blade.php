@extends('admin.master')

@section('title')
Add Product list
@endsection

@section('style')
    <link href="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />
    <link href="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <h5 class="font-strong mb-4">Product List</h5>
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Type:</label>
                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                        <option value="">All</option>
                            @foreach ($categories as $category)
                                <option>{{  $category->name }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    <a class="btn btn-rounded btn-primary btn-air" href="{{ route('admin.product.create') }}">Add Product</a>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="products-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Product</th>
                            <th>Product Show</th>
                            <th>Size</th>
                            <th>Color</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Offer Price</th>
                            <th>Quantity</th>
                            <th style="text-align:center; width: 106px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1;
                        @endphp
                        @foreach ($products as $product)
                        <tr role="row" class="odd">
                            <td>{{ $i++ }}</td>

                            <td>
                                @if(!empty($product->image))
                                    <img class="mr-3" src="{{ asset('uploads/productImage/'.$product->image) }}" alt="image" width="60" />
                                    @else
                                    <img class="mr-3" src="{{ asset('uploads/ProductImage/notFound.jpg') }}" alt="image" width="60" />
                                @endif
                                {{ $product->name }}
                            </td>


                            <td>{{ @$product->product_show_position->name }}</td>

                            <td>
                                @foreach (json_decode($product->color_id) as $color_id)
                                    @foreach(App\Models\Color::where('id', $color_id)->get() as $color)
                                        <button style="background:{{ $color->hexadecimal }} ;width: 20px;height: 20px;" class="btn btn-primary btn-icon-only btn-circle" data-toggle="button">
                                            <span class="active-hidden"></span>
                                        </button>
                                    @endforeach
                                @endforeach
                            </td>
                            <td>
                                @foreach (json_decode($product->size_id) as $size_id)
                                    @foreach(App\Models\Size::where('id', $size_id)->get() as $size)
                                        {{ $size->name }},
                                    @endforeach
                                @endforeach
                            </td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->offerPrice }}</td>
                            <td>
                                @if($product->quantity > 0)
                                    @if($product->quantity <= 5)
                                        <span class="badge badge-warning badge-pill">Low of stock</span>
                                    @else
                                        {{ $product->quantity }}
                                    @endif

                                @else
                                    <span class="badge badge-danger badge-pill">Out of stock</span>
                                @endif
                            </td>


                            <td class="text-center">
                                <a href="{{ route('admin.product.edit', $product->id) }}">
                                    <button class="btn btn-soc-facebook btn-icon-only"><i class="la la-pencil"></i></button>
                                </a>
                                <a href="{{ route('admin.product.destroy', $product->id) }}" onclick="deleteConfirm()">
                                    <button class="btn btn-soc-youtube btn-icon-only"><i class="la la-trash"></i></button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('backend') }}/assets/js/scripts/sweetalert-demo.js"></script>
    <script>
        $(function() {
            $('#products-table').DataTable({
                pageLength: 10,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
            });
            var table = $('#products-table').DataTable();
            $('#key-search').on('keyup', function() {
                table.search(this.value).draw();
            });
            $('#type-filter').on('change', function() {
                table.column(3).search($(this).val()).draw();
            });
        });
    </script>
@endsection
