<div class="form-group  row">
    <label class="col-sm-2 col-form-label">Image</label>
    <div class="col-sm-10">
        <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
            <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
        </label>
    </div>
    <label class="col-sm-2 col-form-label"></label>
    <label class="col-sm-2 col-form-label">
        <div id="targetLayer">
            @if(!empty($product->image))
            <img src="{{ asset('uploads/productImage/'.$product->image) }}" width="200px" height="200px" />
            @endif
        </div>
    </label>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Name {!! required() !!}</label>
    <div class="col-sm-10">
        <input class="form-control" type="text" name="name" value="{{ old('name', $product->name ?? '') }}">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Cagetory {!! required() !!}</label>
    <div class="col-sm-10">
        <select name="category_id" class="form-control">
            <option>Select category</option>
            @foreach ($categorys as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Brand {!! required() !!}</label>
    <div class="col-sm-10">
        <select name="brand_id" class="form-control">
            <option>Select Brand</option>
            @foreach ($brands as $brand)
            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Quantity {!! required() !!}</label>
    <div class="col-sm-10">
        <input class="form-control" type="number" name="quantity" value="{{ old('quantity', $product->quantity ?? '') }}">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Price {!! required() !!}</label>
    <div class="col-sm-10">
        <input class="form-control" type="number" name="price" value="{{ old('price', $product->price ?? '') }}">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Offer Price</label>
    <div class="col-sm-10">
        <input class="form-control" type="number" name="offer_price" value="{{ old('offer_price', $product->offerPrice ?? '') }}">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Description </label>
    <div class="col-sm-10">
        <textarea name="desc">{{ old('desc', $product->desc ?? '') }}</textarea>
        <script>
            CKEDITOR.replace('desc');
        </script>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Product Sizes {!! required() !!}</label>
    <div class="col-sm-10">
        @if($url = action('Admin\ProductController@create') == url()->full())
            @foreach ($sizes as $size)
                <label class="checkbox checkbox-grey checkbox-primary">
                    <input type="checkbox" value="{{ $size->id }}" name="size_id[]">
                    <span class="input-span"></span>{{ $size->name }}
                </label>
            @endforeach
        @else
            @foreach (json_decode($product->size_id) as $size_id)
                @foreach(App\Models\Size::where('id', $size_id)->get() as $size)
                <label class="checkbox checkbox-grey checkbox-primary">
                    <input type="checkbox" value="{{ $size->id }}" name="size_id[]" checked>
                    <span class="input-span"></span>{{ $size->name }}
                </label>
                @endforeach
            @endforeach
            <br>
            @foreach ($sizes as $size)
            <label class="checkbox checkbox-grey checkbox-primary">
                <input type="checkbox" value="{{ $size->id }}" name="size_id[]">
                <span class="input-span"></span>{{ $size->name }}
            </label>
            @endforeach
        @endif
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">Product Colors {!! required() !!}</label>
    <div class="col-sm-10">
        <div class="form-group">
            @if($url = action('Admin\ProductController@create') == url()->full())
            @foreach ($colors as $color)
                <label class="checkbox checkbox-grey checkbox-primary">
                    <input type="checkbox" value="{{ $color->id }}" name="color_id[]">
                    <span class="input-span" style="background:{{ $color->hexadecimal }}"></span>{{ $color->name }}
                </label>
            @endforeach
            @else
                @foreach (json_decode($product->color_id) as $color_id)
                    @foreach(App\Models\Color::where('id', $color_id)->get() as $color)
                    <label class="checkbox checkbox-grey checkbox-primary">
                        <input type="checkbox" value="{{ $color->id }}" name="color_id[]" checked>
                        <span class="input-span" style="background:{{ $color->hexadecimal }}"></span>{{ $color->name }}
                    </label>
                    @endforeach
                @endforeach
                <br>
            @foreach ($colors as $color)
                <label class="checkbox checkbox-grey checkbox-primary">
                    <input type="checkbox" value="{{ $color->id }}" name="color_id[]">
                    <span class="input-span" style="background:{{ $color->hexadecimal }}"></span>{{ $color->name }}
                </label>
            @endforeach
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Product Show Position </label>
    <div class="col-sm-4">
        <select name="product_show" id="product_show" class="form-control">
            <option value="0">Select Brand</option>
            {{ $product_shows }}
            @foreach ($product_shows as $product_show)
            <option value="{{ $product_show->id }}">{{ $product_show->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <div class="col-2">Active {!! required() !!}</div>
    <div class="col-3">
        <label class="ui-switch">
            <input type="checkbox" name="is_active" value="1" {!! is_active(@$product->is_active) !!}>
            <span></span>
        </label>
    </div>
</div>
