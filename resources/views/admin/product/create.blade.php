@extends('admin.master')

@section('title')
Add new Product
@endsection

@section('style')
<link href="{{ asset('backend') }}/assets/vendors/multiselect/css/multi-select.css" rel="stylesheet" />
<link href="{{ asset('backend') }}/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
@endsection

@section('content')

<!-- START PAGE CONTENT-->
<div class="page-heading">
    <h1 class="page-title">Add New Product</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="fa fa-bars"></i></a>
        </li>
        <li class="breadcrumb-item">Product</li>
        <li class="breadcrumb-item">Add New Product</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Add New Product</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="ti-angle-down"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    @include('admin.massages.msg')
                </div>
            </div>

            <form method="POST" action="{{ route('admin.product.store') }}" class="form-horizontal" id="form-sample-1"
                novalidate="novalidate" enctype="multipart/form-data">
                @csrf
                {{--  include form   --}}
               @include('admin.product.form')

               <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-primary btn-fix">
                            <span class="btn-icon">Submit</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @endsection


@section('js')
    <!-- PAGE LEVEL PLUGINS-->
    <script src="{{ asset('backend') }}/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/moment/min/moment.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/multiselect/js/jquery.multi-select.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="{{ asset('backend') }}/assets/js/scripts/form-plugins.js"></script>
@endsection





