@extends('admin.master')

@section('title')
Add Product list
@endsection

@section('style')
    <link href="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />
    <link href="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <div class="d-flex justify-content-between mb-5 pb-4">
                <div class="d-flex align-items-center">
                    <span class="mr-4 static-badge badge-pink">AC</span>
                    <div>
                        <h5 class="font-strong">Adminca</h5>
                        <div class="text-light">Emma Johnson, 17.05.2018, Shipped</div>
                    </div>
                </div>
                <div class="text-right" style="width: 260px;">
                    <h4 class="font-strong mb-3">INVOICE
                        <span class="text-muted font-normal ml-3">160-001F</span>
                    </h4>
                    <div class="row align-items-center mb-2">
                        <div class="col-6 text-muted">Invoice Date</div>
                        <div class="col-6">10 April 2018</div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-6 text-muted">Issue Date</div>
                        <div class="col-6">30 April 2018</div>
                    </div>
                </div>
            </div>
            <div class="row mb-5 pb-3">
                <div class="col-6">
                    <h5 class="font-strong mb-3">INVOICE FROM</h5>
                    <h5 class="font-strong text-success mb-3">Adminca, Inc.</h5>
                    <div class="text-light">San Francisco, CA 94103 Market Street</div>
                    <div class="text-light">adminca@exmail.com</div>
                    <div class="text-light">(123) 456-2112</div>
                </div>
                <div class="col-6 text-right">
                    <h5 class="font-strong mb-3">INVOICE TO</h5>
                    <h5 class="font-strong text-success mb-3">{{ $customers->name }}</h5>
                    <div class="text-light">{{ $customers->address }}</div>
                    <div class="text-light">{{ $customers->email }}</div>
                    <div class="text-light">01752</div>
                </div>
            </div>
            <div class="ibox-fullwidth-block">
                <table class="table mb-4">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th class="pl-4">Item Description</th>
                            <th>Quantity</th>
                            <th>Unit Cost</th>
                            <th class="text-right pr-4">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total;
                        @endphp
                        @foreach ($invoices as $invoice)
                            <tr>
                                <td class="pl-4">
                                    <div><strong class="text-success">{{ $invoice->product->name }}</strong></div><small>{{ $invoice->product->des }}</small></td>
                                <td>{{ $invoice->product_quantity }}</td>
                                <td>{{ $invoice->product->price }}</td>
                                <td class="text-right pr-4">{{ $invoice->product_quantity * $invoice->product->price }}</td>
                                @php
                                    @$total += $invoice->product_quantity * $invoice->product->price;
                                @endphp
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="d-flex justify-content-end">
                <div class="text-right" style="width:300px;">
                    <div class="row mb-2">
                        <div class="col-6">Subtotal Price</div>
                        <div class="col-6">{{ $total }}</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-6">Tax 20%:</div>
                        <div class="col-6">{{ $tex = $total/100*10 }}</div>
                    </div>
                    <div class="row font-strong font-20 align-items-center">
                        <div class="col-6">Total Price:</div>
                        <div class="col-6">
                            <div class="h3 font-strong mb-0">{{ $total+$tex }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('backend') }}/assets/js/scripts/sweetalert-demo.js"></script>
    <script>
        $(function() {
            $('#products-table').DataTable({
                pageLength: 10,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
            });
            var table = $('#products-table').DataTable();
            $('#key-search').on('keyup', function() {
                table.search(this.value).draw();
            });
            $('#type-filter').on('change', function() {
                table.column(3).search($(this).val()).draw();
            });
        });
    </script>
@endsection
