@extends('admin.master')

@section('title')
Add Product list
@endsection

@section('style')
<link href="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />
<link href="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
@endsection
@section('content')

<div class="page-content fade-in-up">
    <div class="d-flex align-items-center mb-5">
        <span class="mr-4 static-badge badge-pink"><i class="ti-shopping-cart-full"></i></span>
        <div>
            <h5 class="font-strong">Order #1253</h5>
            <div class="text-light">Emma Johnson, 17.05.2018, Shipped</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-7">
            <div class="ibox">
                <div class="ibox-body">
                    <h5 class="font-strong mb-5">Products List</h5>
                    <table class="table table-bordered table-hover">
                        <thead class="thead-default thead-lg">
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>QTY</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        @if(!empty($product->product->image))
                                            <img class="mr-3" src="{{ asset('uploads/productImage/'.$product->product->image) }}" alt="image" width="60" />
                                        @else
                                            <img class="mr-3" src="{{ asset('uploads/ProductImage/notFound.jpg') }}" alt="image" width="60" />
                                        @endif
                                        {{ $product->name }}
                                    </td>
                                    <td>{{ $product->product->price }}</td>
                                    <td>{{ $product->product_quantity }}</td>
                                    <td>{{ $product->product->price * $product->product_quantity }}</td>
                                    @php
                                        @$total += $product->product->price * $product->product_quantity;
                                    @endphp
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        <div class="text-right" style="width:300px;">
                            <div class="row mb-2">
                                <div class="col-6">Subtotal Price</div>
                                <div class="col-6">{{ $total }}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6">Discount 5%:</div>
                                <div class="col-6">{{ $total-$des = $total/100*5 }}</div>
                            </div>
                            <div class="row font-strong font-20">
                                <div class="col-6">Total Price:</div>
                                <div class="col-6">
                                    <div class="h3 font-strong">{{ $total+$des }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5">
            <div class="ibox">
                <div class="ibox-body">
                    <h5 class="font-strong mb-4">Order Information</h5>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Total Price</div>
                        <div class="col-8 h3 font-strong text-pink mb-0">{{ $total+$des }}</div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Date</div>
                        <div class="col-8">17.05.2018</div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Delivered</div>
                        <div class="col-8">17.05.2018</div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Status</div>
                        <div class="col-8">
                            @if($order->is_complete == PANDING)
                                <span class="badge badge-primary badge-pill">Pending</span>
                            @elseif($order->is_complete == SHIPPED)
                                <span class="badge badge-success badge-pill">Shipped</span>
                            @elseif($order->is_complete == COMPLETED)
                                <span class="badge badge-success badge-pill">Completed</span>
                            @else
                                <span class="badge badge-danger badge-pill">Canceled</span>
                            @endif
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-4 text-light">Payment</div>
                        <div class="col-8">
                            {{ $order->payment->name }}
                            <img src="assets/img/logos/payment/visa.png" alt="image" width="55" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-body">
                    <h5 class="font-strong mb-4">Buyer Information</h5>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Customer</div>
                        <div class="col-8">{{ $customers->name }}</div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Address</div>
                        <div class="col-8">{{ $customers->address }}</div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-4 text-light">Email</div>
                        <div class="col-8">{{ $customers->email }}</div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-4 text-light">Phone</div>
                        <div class="col-8">+380681478544</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div @endsection @section('js') <script src="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.js">
</script>
<script src="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('backend') }}/assets/js/scripts/sweetalert-demo.js"></script>
<script>
    $(function() {
        $('#products-table').DataTable({
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
        });
        var table = $('#products-table').DataTable();
        $('#key-search').on('keyup', function() {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function() {
            table.column(3).search($(this).val()).draw();
        });
    });
</script>
@endsection
