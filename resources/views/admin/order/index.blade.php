@extends('admin.master')

@section('title')
Add Product list
@endsection

@section('style')
    <link href="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.css" rel="stylesheet" />
    <link href="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-body">
            <h5 class="font-strong mb-4">Order List</h5>
            <div class="flexbox mb-4">
                <div class="flexbox">

                    <label class="mb-0 mr-2">Type:</label>
                    <select class="selectpicker show-tick form-control" id="type-filter" title="Please select" data-style="btn-solid" data-width="150px">
                        <option value="">All</option>
                                    {!! product_status() !!}
                        </optgroup>
                    </select>
                </div>
                {{-- <div class="flexbox">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                    </div>
                    <a class="btn btn-rounded btn-primary btn-air" href="{{ route('admin.product.create') }}">Add Product</a>
                </div> --}}
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="products-table">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th>ID</th>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Total Price</th>
                            <th>Status</th>
                            <th>Payment</th>
                            <th>Date</th>
                            <th style="text-align:center; width: 106px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @php
                            $i=1;
                        @endphp
                        @foreach ($orders as $order)
                        <tr role="row" class="odd">
                            <td>{{ $i++ }}</td>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->user->name }}</td>
                            <td>
                                @php
                                    $i = 0;
                                @endphp
                                @foreach (App\Models\Cart::with('product')->where('order_id', $order->id)->get() as $cart)
                                {{ $i++ }}
                                        {{ $cart->product_quantity * $cart->product->price }}
                                @endforeach
                            </td>

                            <td>
                                @if($order->is_complete == PANDING)
                                    <span class="badge badge-primary badge-pill">Pending</span>
                                @elseif($order->is_complete == SHIPPED)
                                    <span class="badge badge-success badge-pill">Shipped</span>
                                @elseif($order->is_complete == COMPLETED)
                                    <span class="badge badge-success badge-pill">Completed</span>
                                @else
                                    <span class="badge badge-danger badge-pill">Canceled</span>
                                @endif
                            </td>
                            <td> {{ $order->payment_id }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary">Small Split Button</button>
                                    <button class="btn btn-primary dropdown-toggle-split dropdown-toggle dropdown-arrow" data-toggle="dropdown" aria-expanded="false"></button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(141px, 29px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="{{ route('admin.order.invoice', $order->id) }}">Invoice</a>
                                        <a class="dropdown-item" href="{{ route('admin.order.order_details', $order->id) }}">Order Details</a>
                                        <a class="dropdown-item" href="javascript:;">Another action</a>
                                        <a class="dropdown-item" href="javascript:;">Something else here</a>
                                    </div>
                                </div>
                            </td>
                            {{-- <td>
                                <div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary">Small Split Button</button>
                                    <button class="btn btn-primary dropdown-toggle-split dropdown-toggle dropdown-arrow" data-toggle="dropdown" aria-expanded="false"></button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(141px, 29px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="javascript:;">Action</a>
                                        <a class="dropdown-item" href="javascript:;">Another action</a>
                                        <a class="dropdown-item" href="javascript:;">Something else here</a>
                                    </div>
                                </div>
                            </td> --}}


                            {{-- <td class="text-center">
                                <a href="{{ route('admin.product.edit', $product->id) }}">
                                    <button class="btn btn-soc-facebook btn-icon-only"><i class="la la-pencil"></i></button>
                                </a>
                                <a href="{{ route('admin.product.destroy', $product->id) }}" onclick="deleteConfirm()">
                                    <button class="btn btn-soc-youtube btn-icon-only"><i class="la la-trash"></i></button>
                                </a>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('backend') }}/assets/vendors/dataTables/datatables.min.js"></script>
    <script src="{{ asset('backend') }}/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('backend') }}/assets/js/scripts/sweetalert-demo.js"></script>
    <script>
        $(function() {
            $('#products-table').DataTable({
                pageLength: 10,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
            });
            var table = $('#products-table').DataTable();
            $('#key-search').on('keyup', function() {
                table.search(this.value).draw();
            });
            $('#type-filter').on('change', function() {
                var data = $(this).val();
                table.column(4).search($(this).val()).draw();
            });
        });
    </script>
@endsection
