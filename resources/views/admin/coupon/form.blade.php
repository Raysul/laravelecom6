


    <div id="main_category">
        <div class="form-group row main_category">
            <label class="col-sm-2 col-form-label" id="change_name">Code {!! required() !!}</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="code" value="{{ old('name', $category->name ?? '') }}">
            </div>
        </div>
    </div>



    <div class="form-group row sub_category">
        <label class="col-sm-2 col-form-label">Type</label>
        <div class="col-sm-10">
            <select class="form-control" name="child_id" id="child_id">
                <option value="">Please Select a Sub </option>


                    <option value="d">dddddd</option>

        </select>
        </div>
    </div>

    <div id="child_category" style="display: none">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" id="change_name">Child Category {!! required() !!}</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="name" value="{{ old('name', $category->name ?? '') }}">
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea name="desc">{{ old('desc', $category->desc ?? '') }}</textarea>
            <script>
            CKEDITOR.replace('desc');
            </script>
        </div>
    </div>
    <div class="form-group row">
    <div class="col-2">Active {!! required() !!}</div>
            <div class="col-3">
                <label class="ui-switch">
                    <input type="checkbox" name="is_active" value="1" {!! is_active(@$category->is_active) !!}>
                    <span></span>
                </label>
            </div>
        </div>

