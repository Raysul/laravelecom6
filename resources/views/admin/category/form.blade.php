<div class="form-group  row">
        <label class="col-sm-2 col-form-label">Image</label>
        <div class="col-sm-10">
            <label title="Upload image file" for="inputImage" class="btn btn-primary" style=" width: 200px;  margin-top: 20px; ">
                <input name="image" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
            </label>
        </div>
        <label class="col-sm-2 col-form-label"></label>
        <label class="col-sm-2 col-form-label">
            <div id="targetLayer">
            @if(!empty($category->image))
                <img src="{{ asset('uploads/categoriesImage/'.$category->image) }}" width="200px" height="200px"/>
            @endif
            </div>
    </label>
</div>


    <div id="main_category">
        <div class="form-group row main_category">
            <label class="col-sm-2 col-form-label" id="change_name">Name {!! required() !!}</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="name" value="{{ old('name', $category->name ?? '') }}">
            </div>
        </div>
    </div>

    <div class="form-group row sub_primary">
        <label class="col-sm-2 col-form-label">Primary Category</label>
        <div class="col-sm-10">
            <select class="form-control" name="parent_id" id="parent_id">
                <option value="">Please Select a Primary Category</option>
            @foreach ($categories as $category)
                @if($category->parent_id == NULL)
                    <option id='childe_id' value="{{ $category->id }}">{{ $category->name }}</option>
                @endif
            @endforeach
        </select>
        </div>
    </div>

    <div class="form-group row sub_category">
        <label class="col-sm-2 col-form-label">Sub Category</label>
        <div class="col-sm-10">
            <select class="form-control" name="child_id" id="child_id">
                <option value="">Please Select a Sub </option>
            @foreach ($categories as $category)
                @if($category->parent_id !== NULL)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endif
            @endforeach
        </select>
        </div>
    </div>

    {{-- <div id="child_category" style="display: none">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" id="change_name">Child Category {!! required() !!}</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" name="name" value="{{ old('name', $category->name ?? '') }}">
            </div>
        </div>
    </div> --}}

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea name="desc">{{ old('desc', $category->desc ?? '') }}</textarea>
            <script>
            CKEDITOR.replace('desc');
            </script>
        </div>
    </div>
    <div class="form-group row">
    <div class="col-2">Active {!! required() !!}</div>
            <div class="col-3">
                <label class="ui-switch">
                    <input type="checkbox" name="is_active" value="1" {!! is_active(@$category->is_active) !!}>
                    <span></span>
                </label>
            </div>
        </div>

