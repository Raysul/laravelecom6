

@if(Session()->has("msg"))
<div class="alert alert-success" role="alert">
    </strong> {{ Session()->get("msg")  }}
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
