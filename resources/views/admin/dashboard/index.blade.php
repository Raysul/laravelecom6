@extends('admin.master')

@section('style')

@endsection

@section('title')
    Adminca bootstrap 4 &amp; angular 5 admin template, Шаблон админки | Dashboard v7
@endsection

@section('content')
    <div class="page-content fade-in-up">
    <div class="row mb-4">
        <div class="col-lg-4 col-md-6">
            <div class="card mb-4">
                <div class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="73" data-bar-color="#18C5A9" data-size="80"
                        data-line-width="8">
                        <span class="easypie-data text-success" style="font-size:28px;"><i
                                class="ti-shopping-cart"></i></span>
                    </div>
                    <div>
                        <h3 class="font-strong text-success">720</h3>
                        <div class="text-muted">TODAY'S ORDERS</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card mb-4">
                <div class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="42" data-bar-color="#5c6bc0" data-size="80"
                        data-line-width="8">
                        <span class="easypie-data text-primary" style="font-size:32px;"><i
                                class="la la-money"></i></span>
                    </div>
                    <div>
                        <h3 class="font-strong text-primary">$5800</h3>
                        <div class="text-muted">TODAY'S PROFIT</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card mb-4">
                <div class="card-body flexbox-b">
                    <div class="easypie mr-4" data-percent="70" data-bar-color="#ff4081" data-size="80"
                        data-line-width="8">
                        <span class="easypie-data text-pink" style="font-size:32px;"><i
                                class="la la-users"></i></span>
                    </div>
                    <div>
                        <h3 class="font-strong text-pink">1250</h3>
                        <div class="text-muted">TODAY'S CUSTOMERS</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-7">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">LATEST ORDERS</div>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-more-alt"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"> <i class="ti-pencil"></i>Create</a>
                            <a class="dropdown-item"> <i class="ti-pencil-alt"></i>Edit</a>
                            <a class="dropdown-item"> <i class="ti-close"></i>Remove</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="flexbox mb-4">
                        <div class="flexbox">

                            @foreach ($orders as $order)
                                @if($order->is_complete == PANDING)
                                    @php
                                        @$pending += 1;
                                    @endphp
                                @elseif($order->is_complete == SHIPPED)
                                    @php
                                        @$shipped += 1;
                                    @endphp
                                @elseif($order->is_complete == COMPLETED)
                                    @php
                                        @$completed += 1;
                                    @endphp
                                @else
                                    @php
                                        @$canceled += 1;
                                    @endphp
                                @endif
                            @endforeach

                            <span class="flexbox mr-3">
                                <span class="mr-2 text-muted">Pending</span>
                                <span class="h3 mb-0 text-primary font-strong">{{ isset($pending) ? $pending : __('0') }}</span>
                            </span>
                            <span class="flexbox">
                                <span class="mr-2 text-muted">Shipped</span>
                                <span class="h3 mb-0 text-success font-strong">{{ isset($shipped) ? $shipped : __('0') }}</span>
                            </span>
                            <span class="flexbox">
                                <span class="mr-2 text-muted">Completed</span>
                                <span class="h3 mb-0 text-success font-strong">{{ isset($completed) ? $completed : __('0') }}</span>
                            </span>
                            <span class="flexbox">
                                <span class="mr-2 text-muted">Canceled</span>
                                <span class="h3 mb-0 text-pink font-strong">
                                    {{ isset($canceled) ? $canceled : __('0') }}
                                </span>
                            </span>
                        </div>
                        <a class="flexbox" href="ecommerce_orders_list.html" target="_blank">VIEW ALL<i class="ti-arrow-circle-right ml-2 font-18"></i></a>
                    </div>
                    <div class="ibox-fullwidth-block">
                        <table class="table table-hover">
                            <thead class="thead-default thead-lg">
                                <tr>
                                    <th class="pl-4">Order ID</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th class="pr-4" style="width:91px;">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td class="pl-4">
                                            <a href="ecommerce_order_details.html" target="_blank">{{ $order->id }}</a>
                                        </td>
                                        <td>{{ $order->name }}</td>
                                        <td>
                                            @foreach (App\Models\Cart::with('product')->where('order_id', $order->id)->get() as $cart)
                                                  {{ $cart->product_quantity * $cart->product->price }}
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($order->is_complete == PANDING)
                                                <span class="badge badge-primary badge-pill">Pending</span>
                                            @elseif($order->is_complete == SHIPPED)
                                                <span class="badge badge-success badge-pill">Shipped</span>
                                            @elseif($order->is_complete == COMPLETED)
                                                <span class="badge badge-success badge-pill">Completed</span>
                                            @else
                                                <span class="badge badge-danger badge-pill">Canceled</span>
                                            @endif
                                        </td>
                                        <td class="pr-4">{{ make_date($order->created_at) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-5">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">PAYMENT SYSTEMS</div>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-more-alt"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"> <i class="ti-pencil"></i>Create</a>
                            <a class="dropdown-item"> <i class="ti-pencil-alt"></i>Edit</a>
                            <a class="dropdown-item"> <i class="ti-close"></i>Remove</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <ul class="media-list media-list-divider">
                        <li class="media">
                            <div class="media-img">
                                <img src="assets/img/logos/payment/visa.png" alt="image" width="60" />
                            </div>
                            <div class="media-body">
                                <div class="media-heading">Visa card
                                    <h4 class="font-strong float-right text-right"><sup>$</sup>1100</h4>
                                </div>
                                <p class="font-13 m-0 text-light">Lorem ipsum dolar set...</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-img">
                                <img src="assets/img/logos/payment/paypal.png" alt="image" width="60" />
                            </div>
                            <div class="media-body">
                                <div class="media-heading">PayPal
                                    <h4 class="font-strong float-right text-right"><sup>$</sup>985</h4>
                                </div>
                                <p class="font-13 m-0 text-light">Lorem ipsum dolar set...</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-img">
                                <img src="assets/img/logos/payment/mastercard.png" alt="image" width="60" />
                            </div>
                            <div class="media-body">
                                <div class="media-heading">MasterCard
                                    <h4 class="font-strong float-right text-right"><sup>$</sup>850</h4>
                                </div>
                                <p class="font-13 m-0 text-light">Lorem ipsum dolar set...</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-img">
                                <img src="assets/img/logos/payment/jcb.png" alt="image" width="60" />
                            </div>
                            <div class="media-body">
                                <div class="media-heading">JCB
                                    <h4 class="font-strong float-right text-right"><sup>$</sup>690</h4>
                                </div>
                                <p class="font-13 m-0 text-light">Lorem ipsum dolar set...</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-img">
                                <img src="{{ asset('backend/') }}asset/img/logos/payment/eps.png" alt="image" width="60" />
                                http://laravelecom6.ra/backend//assets/img/users/admin-image.png"
                            </div>
                            <div class="media-body">
                                <div class="media-heading">EPS
                                    <h4 class="font-strong float-right text-right"><sup>$</sup>520</h4>
                                </div>
                                <p class="font-13 m-0 text-light">Lorem ipsum dolar set...</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
    <script src="{{ asset('backend') }}/assets/js/scripts/dashboard_7.js"></script>
@endsection
