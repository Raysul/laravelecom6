<!doctype html>
<html dir="ltr" lang="en-US">

<!-- Mirrored from themeforest.stw-services.com/EShop/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Mar 2018 07:34:08 GMT -->

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="EShop" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>@yield('title')</title>
    <link href="{{ asset('frontend') }}/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/style.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/overright.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/colors.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend') }}/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend') }}/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend') }}/css/swiper.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend') }}/css/custom.css" rel="stylesheet" type="text/css"/>

    <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

{{-- <!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/> --}}

<!--Alertifyjs CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/default.min.css"/>
{{-- <!--
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.rtl.min.css"/> --}}
</head>

<body class="blue">
    <!--------------Start wrapper-------------->
    <div id="wrapper" class="clearfix">
        <!--------------Top bar-------------->
        <div id="top-bar">

            <div class="container clearfix">
                <!--Social icons-->
                <div class="top-social-icons">
                    <ul class="clearfix">
                        <li><a class="i-facbook" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="i-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="i-google" href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="i-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                        <li class="dropdown">
                            <a class="i-contact dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-phone"></i>
                            </a>
                            <div class="i-hover dropdown-menu">
                                <span>111 1111 11</span>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a class="i-email dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user"></i>
                            </a>
                            <div class="i-hover dropdown-menu">
                                <span>support@stw-store.com</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--End social icon-->

                <!-- Top link -->
                <div class="top-links clearfix fright">

                    <ul>
                        @guest
                        <li class="dropdown">
                            <a href="#"><i class="fa fa-heart"></i>My Wishlist</a>
                        </li>
                        <li class="dropdown">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="dropdown">
                            <a href="{{ route('register') }}">Sign up</a>
                        </li>
                        @endif
                        @else
                        <li class="dropdown">
                            <a href="#"><i class="fa fa-heart"></i>My Wishlist</a>
                        </li>
                        <li class="dropdown">

                            {{-- @if(App\Models\Customer::where('user_id', Auth::id())->first())
                                <img class="profile" src="{{ asset('uploads/profile/'.App\Models\Customer::where('user_id', Auth::id())->first()->image) }}" alt="Profile"/>
                            @else
                                <img class="profile" src="{{ asset('uploads/profile/profile.png') }}" alt="Profile"/>
                            @endif --}}
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('user.index') }}">My Account</a></li>
                                <li><a href="">My Wishlist</a></li>
                                <li><a href="#">Checkout</a></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                <li>
                                    <a href="#">
                                        Currency
                                        <span class="dropdown-top">USD</span>
                                    </a>
                                    <ul>
                                        <li><a href="#">Usd Dollar</a></li>
                                        <li><a href="#">Euro</a></li>
                                        <li><a href="#">Pound St.</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        Language
                                        <span class="dropdown-top">eng</span>
                                    </a>
                                    <ul>
                                        <li><a href="#">English<img src="{{ asset('frontend') }}/images/flags/england.jpg" alt="England"></a></li>
                                        <li><a href="#">Spanish<img src="{{ asset('frontend') }}/images/flags/spain.jpg" alt="Spain"></a></li>
                                        <li><a href="#">French<img src="{{ asset('frontend') }}/images/flags/france.jpg" alt="France"></a></li>
                                        <li><a href="#">German<img src="{{ asset('frontend') }}/images/flags/germany.jpg" alt="Germany"></a></li>
                                        <li><a href="#">Italian<img src="{{ asset('frontend') }}/images/flags/italy.jpg" alt="Italy"></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        @endguest

                    </ul>
                </div>
                <!-- End top link -->

            </div>

        </div>
        <!--------------End top bar-------------->
        <!--------------Header-------------->
        <header id="header">
            <div class="header-wrapper">
                <div class="container clearfix">
                    <!-- logo -->
                    <div class="logo">
                        <h1 class="clearfix">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('frontend') }}/images/logo-white.png" alt="logo" />
                            </a>
                        </h1>
                    </div>
                    <!--End logo -->
                    <div class="fright clearfix">
                        <div class="right-wrapper">
                            <!--<div class="wb-block">
                            	<span>Welcome to EShop</span>
                            </div>-->
                            <div class="search">
                                <!--Search-->
                                <a href="#"><i class="fa fa-search"></i></a>
                                <form action="#" class="search-form">
                                    <input type="search" name="s" class="s" placeholder="Search entry site here...">
                                    <a href="#" class="search-close"><i class="fa fa-close"></i></a>
                                </form>
                            </div>
                            <!--End Search-->
                            <div class="cart">
                                <!--Start Cart-->
                                <a href="#">
                                    <i class="fa fa-shopping-cart dropdown-toggle" data-toggle="dropdown"></i>
                                    <span class="cart-block pull-right">
                                        <span class="cart-num">{{ App\Models\Cart::totalItems() }}</span>
                                    </span>
                                </a>
                                <div class="dropdown-menu">

                                    {{-- <p>2 item(s) in your cart - $956.00</p> --}}
                                    @foreach (App\Models\Cart::totalCarts() as $cart)
                                        @foreach (App\Models\Product::where('id', $cart->product_id)->get() as $product)
                                            <div class="product clearfix">
                                                <a href="javascript:;" onclick="cart_delete({{ $cart->id }})"><i class="fa fa-close"></i></a>
                                                <figure class="product-images">
                                                    <a href="javascript:;">
                                                        <img src="{{ asset('frontend') }}/images/products/img-1.jpg" alt="" />
                                                    </a>
                                                </figure>
                                                <div class="product-detail">
                                                    <h5 class="product-name">
                                                        <a href="#">{{ $product->name }}</a>
                                                    </h5>
                                                    <div class="product-rate">
                                                        <span>{{ $cart->product_quantity }} x {{ $product->price }} = {{ $cart->product_quantity * $product->price }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @php
                                                @$total_price += $cart->product_quantity * $product->price;
                                            @endphp
                                        @endforeach
                                    @endforeach

                                    <div class="clearfix">
                                        <ul class="pull-left action-info">
                                            <li>
                                                Shopping:
                                                <span>$5.00</span>
                                            </li>
                                            <li>
                                                Text:
                                                <span>Free</span>
                                            </li>
                                            <li>
                                                Total:
                                                <span>${{ @$total_price }}</span>
                                            </li>
                                        </ul>
                                        <ul class="pull-right action-btn">
                                            <li><a href="{{ route('carts') }}">Cart</a></li>
                                            <li><a href="#">Checkout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--End Cart-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="navigation">
                <!-- Start navigation -->
                <div class="container">
                    <nav class="main-nav">
                        <div class="reponsive-menu">
                            <a id="responsive-btn" href="#">
                                <!-- Responsive nav button -->
                                <span class="responsive-btn-icon">
                                    <span class="responsive-btn-block"></span>
                                    <span class="responsive-btn-block"></span>
                                    <span class="responsive-btn-block last"></span>
                                </span>
                                <span class="responsive-btn-text">Menu</span>
                            </a>
                            <!--End responsive nav button -->
                            <div id="responsive-menu-container">

                            </div><!-- End responsive menu container -->
                        </div>
                        <ul class="menu clearfix">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            @foreach ($categoriess as $category)
                            <li><a href="{{ route('category', $category->slug) }}">{{ $category['name'] }}</a>
                                @if(App\Models\Category::where('parent_id',$category->id)->count() > 0)
                                <div class="sub-menu">
                                    <ul>
                                    @foreach (App\Models\Category::where('parent_id',$category->id)->get() as $sub_category)
                                            <li><a href="{{ route('category', $sub_category->slug) }}">{{ $sub_category->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="offer"><a href="#">Todays offer</a></div>
                </div>
            </div><!-- End navigation -->
        </header>
        <!--------------End header-------------->
        @yield('content');
        <!-------------- Footer -------------->
        <div class="wrapper-footer clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="recent-post clearfix">
                            <h4>Recent Post</h4>
                            <div class="recent-item  clearfix">
                                <ul class="post_meta clearfix">
                                    <li class="date-time">
                                        <span class="date">4</span>
                                        <span class="month">May</span>
                                    </li>
                                    <li class="post_detail">
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur adipisci</a>
                                        <span>Post by <a href="#">admin</a></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="recent-item  clearfix">
                                <ul class="post_meta clearfix">
                                    <li class="date-time">
                                        <span class="date">3</span>
                                        <span class="month">May</span>
                                    </li>
                                    <li class="post_detail">
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur adipisci</a>
                                        <span>Post by <a href="#">admin</a></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="recent-item  clearfix">
                                <ul class="post_meta clearfix">
                                    <li class="date-time">
                                        <span class="date">5</span>
                                        <span class="month">May</span>
                                    </li>
                                    <li class="post_detail">
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur adipisci</a>
                                        <span>Post by <a href="#">admin</a></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                        <h4>Information</h4>
                        <ul class="clearfix">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Orders and Returns</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                        <h4>Why choose us</h4>
                        <ul class="clearfix">
                            <li><a href="#">Product Recall</a></li>
                            <li><a href="#">Gift Vouchers</a></li>
                            <li><a href="#">Returns and Exchanges</a></li>
                            <li><a href="#">Shipping Options</a></li>
                            <li><a href="#">Help & FAQs</a></li>
                            <li><a href="#">Hard to Find Parts</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 fa-block">
                        <h4>About us</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>
                        <ul>
                            <li class="add"><i class="fa fa-home"></i>
                                <p>404, Pentagon 1, Magarpatta Zip Code- 411028.</p>
                            </li>
                            <li class="mail"><i class="fa fa-envelope"></i>
                                <p> support@stw-store.com</p>
                            </li>
                            <li class="phone"><i class="fa fa-phone"></i>
                                <p>+000 0000 00</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="spacer15"></div><!-- Spacer -->
                <div class="newsletter-main">
                    <div class="row">
                        <div class=" col-sm-4 sl-block">
                            <h4>Follow us</h4>
                            <div class="social-links">
                                <a href="#"><img src="{{ asset('frontend') }}/images/i-1.png" alt="" /></a>
                                <a href="#"><img src="{{ asset('frontend') }}/images/i-2.png" alt="" /></a>
                                <a href="#"><img src="{{ asset('frontend') }}/images/i-3.png" alt="" /></a>
                                <a href="#"><img src="{{ asset('frontend') }}/images/i-4.png" alt="" /></a>
                                <a href="#"><img src="{{ asset('frontend') }}/images/i-5.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="newsletter">
                                <label>Newsletter</label>
                                <form action="{{ route('subscribe') }}" method="POST">
                                   @csrf
                                    <input type="text" name="email" value="">
                                    <input type="submit" value="SUBSCRIBE">
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer15"></div><!-- Spacer -->
                <div class="f-categories clearfix">
                    <div class="col-sm-12">
                        <h4>Product Categories</h4>
                        <ul>
                            <li><a href="#">Bag</a></li>
                            <li><a href="#">Men's Bags</a></li>
                            <li><a href="#">Women's Bags</a></li>
                            <li><a href="#">Clothing</a></li>
                            <li><a href="#">Dress</a></li>
                            <li><a href="#">Pants</a></li>
                            <li><a href="#">Shirt</a></li>
                            <li><a href="#">T-Shirt</a></li>
                            <li><a href="#">Electronics</a></li>
                            <li><a href="#">Head phone</a></li>
                            <li><a href="#">Phone</a></li>
                            <li><a href="#">Laptop</a></li>
                            <li><a href="#">electronics</a></li>
                            <li><a href="#">Health & Beauty</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="wrapper-copy">
                <div class="container">
                    <div class="copy">
                        <div class="row">
                            <div class="col-sm-8">
                                <p>© 2016 STW Themes Demo Store. All Rights Reserved. Designed by <a href="#">stw-store.Com</a></p>
                            </div>
                            <div class="col-sm-4">
                                <div class="sc-link">
                                    <a class="visa" href="#"></a>
                                    <a class="master" href="#"></a>
                                    <a class="paypal" href="#"></a>
                                    <a class="express" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-------------- End footer -------------->
    </div>
    <!-------------- End wrapper main -------------->
    </div>
    <!-------------- End wrapper -------------->
    <div class="visible-sm visible-md visible-lg se-variation">
        <a class="se-variation-btn" href="#"><i class="fa fa-cog"></i></a>
        <ul>
            <li>
                <a id="light" class="s-link-1" href="index-version-1.html">Blue Version</a>
            </li>
            <li>
                <a id="dark" class="s-link-2" href="index-version-2.html">Light Version </a>
            </li>
        </ul>
    </div>
    <!-------------- Java script -------------->
    <script src="{{ asset('frontend') }}/js/jquery.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/bootstrap.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/owl.carousel.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/main.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/bootstrap-select.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/bootstrap.js" type="text/javascript"></script>
    <script src="{{ asset('frontend') }}/js/plugins.js" type="text/javascript"></script>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function addToCart(product_id) {
            $.post("{{url('/api/carts/store')}}", {
                    product_id: product_id
                })

                .done(function(data) {
                    data = JSON.parse(data);
                    if (data.status == 'success') {
                        alertify.set('notifier','position', 'top-right');
                        alertify.success('Item added to the Cart, you can checkout the product. Total Items: ' + data.totalItems + '<br>');
                        $("#totalItems").html(data.totalItems);
                        $("#cart").append("fjjkfldjksf");
                        location.reload();
                    }
                });
        }

        function update_quantity(cart_id){
            var product_quantity = $('#product_quantity').val();
            $.post( "{{url('/api/carts/update')}}",
            {
                cart_id: cart_id,
                product_quantity:product_quantity
            } )

            .done(function( data ) {
                data = JSON.parse(data);
                if ( data.status == 'success'){
                    alertify.set('notifier','position', 'top-right');
                    alertify.success('Item added to the Cart, you can checkout the product. Total Items: ' + data.totalItems + '<br>');
                      $("#totalItems").html(data.totalItems);
                      location.reload()
                }
          });
        }

        function cart_delete(cart_id) {
            $.post("{{route('carts.delete')}}", {
                    cart_id: cart_id,
                })

                .done(function(data) {
                    data = JSON.parse(data);
                    if (data.status == 'success') {
                        alertify.set('notifier','position', 'top-right');
                        alertify.success('Item added to the Cart, you can checkout the product. Total Items: ' + data.id + '<br>');
                        $("#totalItems").html(data.totalItems);
                        $(".cart-num").html(data.id);
                        location.reload()
                    }
                });
        }
//Wishlist
        function wishlist(product_id) {
            alert(product_id);
            $.post("{{url('/api/wishlists/store')}}", {
                    product_id: product_id
                })
                .done(function(data) {
                    data = JSON.parse(data);
                    if (data.status == 'success') {
                        alertify.set('notifier','position', 'top-right');
                        alertify.success('Wishlist Added');
                    }
                });
        }
    </script>
</body>

<!-- Mirrored from themeforest.stw-services.com/EShop/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Mar 2018 07:37:02 GMT -->

</html>
