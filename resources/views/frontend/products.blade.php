@extends('layouts.app')
@section('content')

@php
   foreach ($data as $key => $value) {
        $$key = $value;
   }
@endphp

<div class="wrapper-breadcrumbs clearfix">
    <div class="spacer30"></div><!--spacer-->
       <div class="container">
         <div class="breadcrumbs-main clearfix">
            <h2>{{ $category->name }}</h2>
              <ul>
                  <li><a href="#">Home</a><span class="separator">/ </span></li>

                @if(App\Models\Category::where('parent_id',$category->id)->count() > 0)
                    @foreach (App\Models\Category::where('parent_id',$category->id)->get() as $sub_category)
                        <li><strong>{{ $sub_category->name }}</strong></li>
                    @endforeach
                @endif
                <li><a href="#"> {{ $category->name }} </a><span class="separator">/ </span></li>
            </ul>
         </div>
      </div>
    <div class="spacer15"></div><!--spacer-->
 </div>
 <!--XXXXXXXXXX-- End Wrapper-breadcrumbs --XXXXXXXXXX-->
<!--XXXXXXXXXX-- Start Wrapper Main XXXXXXXXXX-->
<div class="wrapper-main brandshop clearfix">
    <div class="spacer15"></div><!--spacer-->
    <div class="container">
        <div class="inner-block"><!------Main Inner-------->
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="main-contant clearfix">
                        <div class="contant-wrapper">
                            <div class="image-category">
                                <img src="{{ asset('uploads/categoryImage/'.$category->image) }}" alt=""/>
                            </div>

                            <div class="products-grid clearfix"><!-- Start Product List -->
                                <div class="row">
                                    <div class="item-block clearfix">
                                        <div class="grid-control clearfix">
                                            @foreach ($products as $product)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$product->id)}}"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt=""/></a>
                                                            <a class="add-to-cart" href="javascript:void(0)" onclick="addToCart({{ $product->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $product->id }})"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{ $product->name }}
                                                            </a>
                                                            <span>${{ $product->price }}</span>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @endforeach

                                        </div>
                                        <div class="spacer30"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="toolbar clearfix">
                                                    <div class="pager"><!--pagination -->
                                                        {{ $products->links() }}
                                                    </div><!-- End pagination -->
                                                    {{-- <div class="sorter"><!--pagination-info -->
                                                        <p>Items 1 to 16 of 73 total</p>
                                                    </div><!-- End pagination-info --> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Product List -->
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="side-bar clearfix"><!--Side Bar-->
                        <div class="aside compare">
                            <h4 class="heading">Compare Products</h4>
                            <div class="compare-main">
                                <p>You have no items to compare.</p>
                            </div>
                        </div>
                        <div class="aside categories"><!--Side Categories-->
                             <h4 class="heading">Categories</h4>
                             <ul class="categories-main">
                                @foreach ($categoriess as $category)
                                    @if(App\Models\Category::where('parent_id',$category->id)->count() > 0)
                                        <a role="button" data-toggle="collapse" href="#link1" aria-expanded="false" aria-controls="link1">{{ $category->name }}<i class="pull-right fa fa-plus"></i></a>
                                            <li>
                                                <ul class="collapse" id="link1">
                                                    @foreach (App\Models\Category::where('parent_id',$category->id)->get() as $sub_category)
                                                        <li><a href="{{ route('category', $sub_category->slug) }}">{{ $sub_category->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div><!--End Side Categories-->
                        <div class="aside shop-by" ><!--Shop By-->
                            <h4 class="heading">Brond</h4>
                            <h5>Manufacture</h5>
                            <ul>
                                @foreach ($brands as $brand)
                                    <li><a href="#">{{ $brand->name }}</a></li>
                                @endforeach
                            </ul>
                            <h5>Price</h5>
                            <div class="price-box">
                                <div id="price_range"></div>
                                {{-- <form action="{{ route('products') }}" method="get" id="frm_price_range" name=""> --}}
                                    <input type="number" name="min_price" id="min_price" value="10" /> -
                                    <input type="number" name="max_price" id="max_price" value="1000" min="0" max="1500" />
                                    <button type="submit" class="btn grey">FILTER</button>
                                {{-- </form> --}}
                            </div>
                            <h5>Color</h5>
                            <ul>
                                <li><a href="#">White(10)</a></li>
                                <li><a href="#">Black(15)</a></li>
                                <li><a href="#">Blue(20)</a></li>
                                <li><a href="#">Red(52)</a></li>
                                <li><a href="#">Green(2)</a></li>
                            </ul>
                            <h5>Size</h5>
                            <ul>
                                <li><a href="#">Small(20)</a></li>
                                <li><a href="#">X - Small(15)</a></li>
                                <li><a href="#">Medium Voitton(5)</a></li>
                                <li><a href="#">Large(90)</a></li>
                            </ul>
                        </div><!--Shop By-->
                        <div class="aside tag"><!-- Start Tag -->
                            <h4 class="heading">Tags</h4>
                            <div class="tag-main">
                                <a href="#" class="button tag">Shop</a>
                                <a href="#" class="button tag">Jeen</a>
                                <a href="#" class="button tag">Jacket</a>
                                <a href="#" class="button tag">Fashion</a>
                                <a href="#" class="button tag">Men Store</a>
                                <a href="#" class="button tag">Women</a>
                                <a href="#" class="button tag">Footwear</a>
                                <a href="#" class="button tag">Dress</a>
                            </div>
                        </div><!-- End Tags -->
                        <div class="aside compare"><!-- Start Compare -->
                            <h4 class="heading">Compare</h4>
                            <div class="compare-main">
                                <div class="owl-carousel compare-slide">
                                    <a href="#"><img src="{{ asset('frontend') }}/images/products/img-1.jpg" alt=""/></a>
                                    <a href="#"><img src="{{ asset('frontend') }}/images/products/img-1.jpg" alt=""/></a>
                                    <a href="#"><img src="{{ asset('frontend') }}/images/products/img-1.jpg" alt=""/></a>
                                </div>
                            </div>
                        </div><!-- End Compare -->
                    </div><!--End Side Bar-->
                </div>
            </div>
        </div>
    </div>
    <div class="spacer30"></div><!--spacer-->
    <!--XXXXXXXXXX-- Brand Wrapper --XXXXXXXXXX-->
    <div class="brand-wrapper">
        <div class="container">
            <div class="brand-main">
                <div class="owl-carousel brand">
                    <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-1%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-2%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-3%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-4%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-5%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-6%20-%20Copy.png" alt=""/></a>
                       <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-7%20-%20Copy.png" alt=""/></a>
                 </div>
             </div>
        </div>
    </div>
@endsection
