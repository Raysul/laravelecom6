@extends('layouts.app')
@section('content')

<div class="wrapper-main brandshop clearfix">
 <div class="spacer15"></div><!--spacer-->
 <div class="container">
         <div class="row">
             @include('frontend.users.sidebar')
             <div class="col-md-6 col-lg-9 col-sm-6">
                <div class="aside categories"><!--Side Categories-->
                    <h2 class="heading text-center">Manage My Account</h2>
                    <ul class="categories-main">
                       <li class="text-center">
                       <a href="{{route('user.edit')}}">Personal Profile Edit</a>
                       </li>
					<li class="text-info text-center">{{$user->name}}</li>
                       <li class="text-info text-center">{{$user->email}}</li>
                   </ul>
               </div>
             </div>
         </div>
</div>
 </div>
@endsection

