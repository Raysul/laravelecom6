@extends('layouts.app')
@section('content')

<div class="wrapper-main brandshop clearfix">
    <div class="spacer15"></div>
    <!--spacer-->
    <div class="container">
        <div class="row">
            @include('frontend.users.sidebar')
            <div class="col-sm-6 contact-form">
                <div class="new-account">
                    <div class="sign-block">
                        <h4>Edit Profile</h4>
                        @include('frontend.msg')
                        <form method="POST" action="{{ route('user.update') }}" enctype="multipart/form-data">
                            @csrf

                            <ul>
                                <li class="form-group">
                                    <label for="name">Name {!! required() !!}</label>
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ $user->name }}" />
                                </li>
                                @if ($errors->has('name'))
                                <span>
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    <label for="phone">Phone Number {!! required() !!}</label>
                                    <input type="text" id="phone" name="phone" class="form-control" value="{{ isset($user->customer->phone) ? $user->customer->phone : "" }}" required/>
                                </li>
                                @if ($errors->has('phone'))
                                <span>
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    <label for="email">Email {!! required() !!}</label>
                                    <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}" required/>
                                </li>
                                @if ($errors->has('email'))
                                <span>
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    <label for="address">Address {!! required() !!}<span></span></label>
                                    <textarea class="form-control" name="address" id="address" cols="30" rows="5" required>
                                        {{ isset($user->customer->address) ? $user->customer->address : "" }}
                                    </textarea>
                                </li>
                                @if ($errors->has('address'))
                                <span>
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    <label for="division">Division {!! required() !!}<span></span></label>
                                    <select class="form-control" name="division_id" id="division_id" required>
                                        <option value="">Select Division</option>
                                        @foreach ($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </li>
                                @if ($errors->has('division'))
                                <span>
                                    <strong>{{ $errors->first('division') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    <label for="district">Districts {!! required() !!}<span></span></label>
                                    <select class="form-control" name="district_id" id="district_id" required>
                                        <option value="">Select District</option>
                                        @foreach ($districts as $district)
                                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                                        @endforeach
                                    </select>
                                </li>
                                @if ($errors->has('district'))
                                <span>
                                    <strong>{{ $errors->first('district') }}</strong>
                                </span>
                                @endif
                                <li class="form-group">
                                    @if(@$user->customer->image)
                                        <img class="profile-upload" src="{{ asset('uploads/profile/'.$user->customer->image) }}" alt="Profile"/>
                                    @endif
                                    <label for="image">Profile Picture {!! required() !!}<span></span></label>
                                    <input type="file" name="image" class="form-control" required>
                                </li>
                                @if ($errors->has('image'))
                                <span>
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif
                                <button type="submit" class="btn btn-lg btn-info">Update Profile</button>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
