@extends('layouts.app')
@section('content')
<!--XXXXXXXXXX-- Start Wrapper Main XXXXXXXXXX-->
<div class="wrapper-main brandshop clearfix">
 <div class="spacer15"></div><!--spacer-->
 <div class="container">
         <div class="row">
             @include('frontend.users.sidebar')
             <div class="col-md-9 col-sm-4">
                <table class="table table-borderless">
                      <tr>
                        <td>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Birthday</th>
                        <th>Gender</th>
                      </tr>
                    <tbody>
                      <tr>
                        <td class="text-info">{{ $user->name }}</td>
                        <td class="text-info">{{ $user->email }}</td>
                        <td class="text-info">{{ isset($user->customer->phone) ? $user->customer->phone : "" }}</td>
                        <td class="text-info">09-03-2000</td>
                        <td class="text-info">Male</td>
                      </tr>
                    </tbody>
                  </table>
                <a href="{{route('user.edit')}}" class="btn btn-lg btn-warning text-info">Edit</a>
             </div>
         </div>
</div>
 </div>
@endsection

