@extends('layouts.app')

@section('content')





<div class="container">
    <!-- Page BradeCrumbs Start -->
    <ul class="breadcrumb">
        <li><a href=""><i class="fa fa-home"></i></a></li>
        <li><a href="">Set on Page</a></li>
        <li><a href="">Set on Page</a></li>
    </ul>
    <!-- Page BradeCrumbs End -->

    <div class="row">

        <div class="card card-body">

            <div class="col-md-6">

                <form method="POST" action="{{ route('user.update') }}">
                    @csrf

                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="name" class="form-control{{$errors->has('name') ? 'is-invalid' : '' }}" value="{{ $user ?? ''->name }}">

                        @if ($errors->has('name'))
                        <span>
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Eamil</label>
                        <input type="text" name="email" class="form-control{{$errors->has('email') ? 'is-invalid' : '' }}" value="{{ $user ?? ''->email }}" required="required">

                        @if ($errors->has('email'))
                        <span>
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address" class="form-control{{$errors->has('address') ? 'is-invalid' : '' }}" value="{{ is_set_value($costomer->address) }}" required="required">

                        @if ($errors->has('address'))
                        <span>
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                    </div>
                    {{-- <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control{{$errors->has('last_name') ? 'is-invalid' : '' }}" value="{{ is_set_value($user ?? ''->last_name) }}" required="required">

                        @if ($errors->has('last_name'))
                        <span>
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control{{$errors->has('username') ? 'is-invalid' : '' }}" value="{{ is_set_value($user ?? ''->username) }}" required="required">

                        @if ($errors->has('username'))
                        <span>
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                    </div> --}}
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control{{$errors->has('email') ? 'is-invalid' : '' }}" value="{{ is_set_value($user ?? ''->email) }}" required="required">

                        @if ($errors->has('email'))
                        <span>
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" class="form-control{{$errors->has('phone') ? 'is-invalid' : '' }}" value="{{ is_set_value($user ?? ''->phone) }}" required="required">

                        @if ($errors->has('phone'))
                        <span>
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    {{-- <div class="form-group">
	  						<label>Division</label>

	  						<select class="form-control" name="division_id">
	  							<option value="">Please Select Your Division</option>
	  							@foreach ( $divisions as $division )
	  								<option value="{{ $division->id }}" {{ $user ?? ''->division_id == $division->id ? 'selected' : '' }}>{{ $division->name }}</option>
                    @endforeach
                    </select>
            </div> --}}


            {{-- <div class="form-group">
	  						<label>District</label>

	  						<select class="form-control" name="district_id">
	  							<option value="">Please Select Your District</option>
	  							@foreach ( $districts as $district )
	  								<option value="{{ $district->id }}" {{ $user ?? ''->district_id == $district->id ? 'selected' : '' }}>{{ $district->name }}</option>
            @endforeach
            </select>
        </div> --}}
        <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" class="form-control{{$errors->has('address') ? 'is-invalid' : '' }}" value="{{ is_set_value($user ?? ''->address) }}" required="required">

            @if ($errors->has('address'))
            <span>
                <strong>{{ $errors->first('address') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group">
            <label>Shipping Address</label>
            <textarea id="shipping_address" class="form-control{{$errors->has('shipping_address') ? 'is-invalid' : '' }}" rows="4" name="shipping_address">{{ is_set_value($user ?? ''->shipping_address) }}</textarea>
            @if ($errors->has('shipping_address'))
            <span>
                <strong>{{ $errors->first('shipping_address') }}</strong>
            </span>
            @endif
        </div>
        <!-- Password -->
        <div class="form-group">
            <label>{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <!-- Confirm Password -->
        <div class="form-group">
            <label>{{ __('Confirm Password') }}</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-dark">
                Update Profile
            </button>
        </div>
        </form>
    </div>
</div>
</div>

</div>



@endsection
