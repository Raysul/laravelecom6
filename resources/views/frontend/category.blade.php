@extends('layouts.app')
@section('content')

@php
   foreach ($data as $key => $value) {
        $$key = $value;
   }
@endphp

<div class="wrapper-breadcrumbs clearfix">
    <div class="spacer30"></div><!--spacer-->
       <div class="container">
         {{-- <div class="breadcrumbs-main clearfix">
            <h2>{{ $category->name }}</h2>
              <ul>
                  <li><a href="#">Home</a><span class="separator">/ </span></li>
                @if(App\Models\Category::where('parent_id',$category->id)->count() > 0)
                    @foreach (App\Models\Category::where('parent_id',$category->id)->get() as $sub_category)
                        <li><strong>{{ $sub_category->name }}</strong></li>
                    @endforeach
                @endif
                <li><a href="#"> {{ $category->name }} </a><span class="separator">/ </span></li>
            </ul>
         </div> --}}
      </div>
    <div class="spacer15"></div><!--spacer-->
 </div>
 <!--XXXXXXXXXX-- End Wrapper-breadcrumbs --XXXXXXXXXX-->
<!--XXXXXXXXXX-- Start Wrapper Main XXXXXXXXXX-->
<div class="wrapper-main brandshop clearfix">
    <div class="spacer15"></div><!--spacer-->
    <div class="container">
        <div class="inner-block"><!------Main Inner-------->
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="main-contant clearfix">
                        <div class="contant-wrapper">
                            <div class="image-category">
                                @if(@$brand->id)
                                    <img src="{{ asset('uploads/brandImage/'.$brand->image) }}" alt=""/>
                                @else
                                    <img src="{{ asset('uploads/categoryImage/'.$category->image) }}" alt=""/>
                                @endif
                            </div>

                            <div class="products-grid clearfix"><!-- Start Product List -->
                                <div class="row">
                                    <div class="item-block clearfix">
                                        <div class="grid-control clearfix">
                                            @foreach ($products as $product)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$product->id)}}"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt=""/></a>
                                                            <a class="add-to-cart" href="javascript:void(0)" onclick="addToCart({{ $product->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $product->id }})"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{ $product->name }}
                                                            </a>
                                                            <span>${{ $product->price }}</span>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @endforeach

                                        </div>
                                        <div class="spacer30"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="toolbar clearfix">
                                                    <div class="pager"><!--pagination -->
                                                        {{ $products->links() }}
                                                    </div><!-- End pagination -->
                                                    {{-- <div class="sorter"><!--pagination-info -->
                                                        <p>Items 1 to 16 of 73 total</p>
                                                    </div><!-- End pagination-info --> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Product List -->
                        </div>
                    </div>
                </div>
                    @include('frontend.partials.sidebar')
                </div>
            </div>
        </div>
    </div>
    <div class="spacer30"></div><!--spacer-->
    <!--XXXXXXXXXX-- Brand Wrapper --XXXXXXXXXX-->
    <div class="brand-wrapper">
        <div class="container">
            <div class="brand-main">
                <div class="owl-carousel brand">
                    <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-1%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-2%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-3%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-4%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-5%20-%20Copy.png" alt=""/></a>
                      <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-6%20-%20Copy.png" alt=""/></a>
                       <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-7%20-%20Copy.png" alt=""/></a>
                 </div>
             </div>
        </div>
    </div>
@endsection
