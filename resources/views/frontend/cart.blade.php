@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                {{-- <li><a href="{{ route('cart') }}">Cart Page</a></li> --}}
            </ul>
        </div>
    </div>

    <div class="inner-block">
        <!------Main Inner-------->
        <div class="row">
            <div class="cart-main">
                <!-- Shopping Cart -->
                <div class="col-md-12">
                    <div class="cart-control clearfix">
                        <table class="table cart-table">
                            <thead>
                                <tr>
                                    <th class="table-title">Product Name</th>
                                    <th class="table-title">Product Code</th>
                                    <th class="table-title">Unit Price</th>
                                    <th class="table-title">Quantity</th>
                                    <th class="table-title">SubTotal</th>
                                    <th><span class="close-button disabled"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $total_price = 0;
                                @endphp
                                @foreach ($carts as $cart)
                                <tr>
                                    <td class="product-name-col">
                                        <figure>
                                            <a href="#"><img src="{{ asset('uploads/productImage/' . $cart->product->image) }}" alt=""></a>
                                        </figure>
                                        <h2 class="product-name">
                                            <a href="#">{{ $cart->product->name }}</a>
                                        </h2>
                                        <ul>
                                            <li>Color: White</li>
                                            <li>Size: SM</li>
                                        </ul>
                                    </td>

                                    <td class="product-price-col"><span class="product-price-special"> {{ $cart->product->price }}</span></td>
                                    <td>
                                        <div class="custom-quantity-input">
                                            <input type="text" name="product_quantity" id="product_quantity" onkeyup="update_quantity({{ $cart->id }})" value="{{ $cart->product_quantity }}">
                                        </div>
                                    </td>
                                    <td class="product-total-col">
                                        <span class="product-price-special">
                                            @php
                                            $total_price += $cart->product->price * $cart->product_quantity;
                                            @endphp
                                            {{ $cart->product->price *  $cart->product_quantity }}
                                        </span>
                                    </td>
                                    <td><a onclick="cart_delete({{ $cart->id }})" class="close-button">
                                            <i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="spacer15"></div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="tab-container left clearfix">
                                    <!-- Tab nav -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="active"><a href="#shipping" data-toggle="tab">Shipping &amp; Taxes</a></li>
                                        <li><a href="#discount" data-toggle="tab">Discount Code</a></li>
                                        <li><a href="#gift" data-toggle="tab">Gift voucher </a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="shipping">
                                            <form action="#" class="clearfix">
                                                <p class="ship-desc">Enter your destination to get a shipping estimate</p>
                                                <div class="ship-row clearfix">
                                                    <span class="ship-label col-3">Country<i>*</i></span>
                                                    <div class="normal-selectbox">
                                                        <select id="country" name="country" class="selectpicker">
                                                            <option value="">Please select</option>
                                                            <option value="Turkey">Turkey</option>
                                                            <option value="Germany">Germany</option>
                                                            <option value="Korea">Korea</option>
                                                        </select>
                                                    </div><!-- End .normal-selectbox-->
                                                </div>
                                                <div class="ship-row clearfix">
                                                    <span class="ship-label col-3">Region/State<i>*</i></span>
                                                    <div class="normal-selectbox">
                                                        <select id="region" name="region" class="selectpicker">
                                                            <option value="">Please select</option>
                                                            <option value="Texas">Texas</option>
                                                            <option value="California">California</option>
                                                        </select>
                                                    </div><!-- End .normal-selectbox-->
                                                </div>
                                                <div class="ship-row clearfix">
                                                    <span class="ship-label col-3">Post Codes<i>*</i></span>
                                                    <div class="col-3 ship-post"><input type="text" class="form-control text-center" value="12315"></div>
                                                    <div class="col-3 text-right"><a href="#" class="btn btn-custom-6 btn-block">Get Quotes</a></div>
                                                </div>
                                            </form>
                                        </div><!-- End .tab-pane -->
                                        <div class="tab-pane fade" id="discount">
                                            <p class="ship-desc">Enter your discount coupon here:</p>
                                            <hr>
                                            <div class="ship-row clearfix">
                                                <span class="ship-label col-3">Discount Code<i>*</i></span>
                                                <div class="col-3-2x"><input type="text" class="form-control" placeholder="coupon here"></div>
                                            </div>
                                            <div class="ship-row clearfix">
                                                <span class="ship-label col-3">Discount Code 2<i>*</i></span>
                                                <div class="col-3-2x"><input type="text" class="form-control" placeholder="coupon here"></div>
                                            </div>
                                            <div class="ship-row">
                                                <a href="#" class="btn btn-custom-5">Activate</a>
                                            </div>
                                        </div><!-- End .tab-pane -->
                                        <div class="tab-pane fade" id="gift">
                                            <p class="ship-desc">Enter your discount coupon here:</p>
                                            <hr>
                                            <div class="ship-row clearfix">
                                                <span class="ship-label col-3">Voucher Code<i>*</i></span>
                                                <div class="col-3-2x"><input type="text" class="form-control" placeholder="coupon here"></div>
                                            </div>
                                            <div class="xs-margin"></div><!-- space -->
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam omnis commodi praesentium non temporibus error eius molestiae cum.</p>
                                        </div><!-- End .tab-pane -->
                                    </div><!-- End .tab-content -->
                                </div><!-- End .tab-container -->
                                <div class="md-margin"></div><!-- space -->
                                <a href="#" class="btn btn-custom btn-lger min-width-lg">Continue Shopping</a>
                            </div><!-- End .col-md-8 -->
                            <div class="col-md-4">
                                <table class="table total-table">
                                    <tbody>
                                        <tr>
                                            <td class="total-table-title">Subtotal:</td>
                                            <td>$434.50</td>
                                        </tr>
                                        <tr>
                                            <td class="total-table-title">Shipping:</td>
                                            <td>$6.00</td>
                                        </tr>
                                        <tr>
                                            <td class="total-table-title">TAX (0%):</td>
                                            <td>$0.00</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td>$440.50</td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="md-margin"></div><!-- space -->
                                <div class="text-right">
                                    <a href="#" class="btn btn-custom btn-lger min-width-sm">Checkout</a>
                                </div>
                            </div><!-- End .col-md-4 -->
                        </div><!-- End .row -->
                    </div>
                </div>
            </div><!-- End Shopping Cart -->
        </div>
    </div>
</div>
@endsection
