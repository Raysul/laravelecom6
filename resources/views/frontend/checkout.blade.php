@extends('layouts.app')

@section('content')

<div class="container">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ route('cart.index') }}">Add to Cart</a></li>
        <li><a href="">Checkout</a></li>
    </ul>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Total Confirm Items</h5>

                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Product Title</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php
                            $total_price = 0;
                            @endphp
                            @foreach ($carts as $cart)
                            <tr>
                                <td>
                                    <h4>{{ $cart->product->title }}</h4>
                                </td>
                                <td>
                                    <h4>{{ $cart->product_quantity }} Pcs</h4>
                                </td>
                                <td>
                                    <h4>={{ $cart->product->price }}/= BDT</h4>
                                </td>
                            </tr>
                            @php
                            $total_price += $cart->product->price * $cart->product_quantity;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>

                    <div>
                        <div class="card" style="width: 33rem; float: right;">
                            <div class="card-body">
                                <h5 class="card-title">Total Amount:</h5>
                                <p class="card-text">
                                    Total Price: <strong>{{ $total_price }} BDT</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <h1 style="text-align: center;">Your Shipping Address</h1>
        <div class="col-md-8 offset-md-2">
            <form action="{{ route('checkout.store') }}" method="POST">
                @csrf

                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="name" class="form-control" value="{{ Auth::check() ? Auth::user()->name : '' }}" required autofocus>
                </div>

                <div class="form-group">
                    <label>Email Address</label>
                    <input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}}" value="{{ Auth::check() ? Auth::user()->email : '' }}" required autofocus>
                    @if ( $errors->has('email') )
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" name="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : ''}}" value="{{ Auth::check() ? Auth::user()->phone : '' }}" required autofocus>
                    @if ( $errors->has('phone') )
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="shipping_address" class="form-control{{ $errors->has('shipping_address') ? ' is-invalid' : '' }}" value="{{ Auth::check() ? Auth::user()->shipping_address : '' }}" required autofocus>
                    @if ( $errors->has('shipping_address') )
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('shipping_address') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="message">Additional Message</label>
                    <textarea id="message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" rows="4" name="message"></textarea>

                    @if ( $errors->has('message') )
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="form-group">
                    <label for="payment_method">Payment Method</label>
                    <select class="form-control" name="payments" id="payments" required="required">
                        <option>Select a Payment Method from the Below List</option>
                        @foreach ($payments as $payment)
                        <option value="{{ $payment->short_name }}">{{ $payment->name }}</option>
                        @endforeach
                    </select>


                    @foreach ($payments as $payment)
                    @if ( $payment->short_name == "cash_in" )
                    <div class="alert alert-success hidden" id="payment_{{ $payment->short_name }}">
                        <h2>For Cash on Delivery, You can complete your Order Process. You will get your Product in three business days.</h2>
                    </div>
                    @else
                    <div class="payment-box hidden" id="payment_{{ $payment->short_name }}">
                        <h2>{{ $payment->name }} Payment</h2>
                        <h4>Send Your Payment at: {{ $payment->payment_no }}<span>{{ $payment->payment_type }}</span></h4>
                        <div class="alert alert-success">
                            Plesae type your Transaction ID in the below field.
                        </div>

                    </div>
                    @endif
                    @endforeach
                    <input type="text" name="transaction_id" id="transaction_id" class="form-control hidden" placeholder="Enter Your Rocket/Bkash Transaction ID">


                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default">Place Your Order Now</button>
                </div>


            </form>
        </div>
    </div>

</div>
<button onchange="test()">Button Submit</button>


@endsection


@section('scripts')

<script type="text/javascript">







    $("#payments").change(function() {
        alert(0);
        $payment_method = $('#payments').val();

        if ($payment_method == "cash_in") {
            $("#payment_cash_in").removeClass('hidden');
            $("#payment_bkash").addClass('hidden');
            $("#payment_rocket").addClass('hidden');
            $("#transaction_id").addClass('hidden');
        } else if ($payment_method == "bkash") {
            $("#payment_cash_in").removeClass('hidden');
            $("#payment_bkash").addClass('hidden');
            $("#payment_rocket").addClass('hidden');
            $("#transaction_id").removeClass('hidden');
        } else if ($payment_method == "rocket") {
            $("#payment_cash_in").removeClass('hidden');
            $("#payment_bkash").addClass('hidden');
            $("#payment_rocket").addClass('hidden');
            $("#transaction_id").removeClass('hidden');
        }

    });


alert('jkljsdk')
</script>
@endsection
