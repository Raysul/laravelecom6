<div class="col-md-3 col-sm-4">
    <div class="side-bar clearfix"><!--Side Bar-->
        <div class="aside compare">
            <h4 class="heading">Compare Products</h4>
            <div class="compare-main">
                <p>You have no items to compare.</p>
            </div>
        </div>
        <div class="aside categories"><!--Side Categories-->
             <h4 class="heading">Categories</h4>
             <ul class="categories-main">
                @foreach ($categoriess as $category)
                    @if(App\Models\Category::where('parent_id',$category->id)->count() > 0)
                        <a role="button" data-toggle="collapse" href="#link1{{ $category->id }}" aria-expanded="false" aria-controls="link1{{ $category->id }}">{{ $category->name }}<i class="pull-right fa fa-plus"></i></a>
                            <li>
                                <ul class="collapse" id="link1{{ $category->id }}">
                                    @foreach (App\Models\Category::where('parent_id',$category->id)->get() as $sub_category)
                                        <li><a href="{{ route('category', $sub_category->slug) }}">{{ $sub_category->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                    @endif
                @endforeach
            </ul>
        </div><!--End Side Categories-->
        <div class="aside shop-by" ><!--Shop By-->
            <h4 class="heading">Brond</h4>
            <h5>Manufacture</h5>
            <ul>
                @foreach ($brands as $brand)
                    <li><a href="{{ route('brand', $brand->slug) }}">{{ $brand->name }}</a></li>
                @endforeach
            </ul>
            {{-- <h5>Price</h5>
            <div class="price-box">
                <div id="price_range"></div> --}}
                {{-- <form action="{{ route('products') }}" method="get" id="frm_price_range" name=""> --}}
                    {{-- <input type="number" name="min_price" id="min_price" value="10" /> -
                    <input type="number" name="max_price" id="max_price" value="1000" min="0" max="1500" />
                    <button type="submit" class="btn grey">FILTER</button> --}}
                {{-- </form> --}}
            {{-- </div> --}}
            {{-- <h5>Color</h5>
            <ul>
                <li><a href="#">White(10)</a></li>
                <li><a href="#">Black(15)</a></li>
                <li><a href="#">Blue(20)</a></li>
                <li><a href="#">Red(52)</a></li>
                <li><a href="#">Green(2)</a></li>
            </ul>
            <h5>Size</h5>
            <ul>
                <li><a href="#">Small(20)</a></li>
                <li><a href="#">X - Small(15)</a></li>
                <li><a href="#">Medium Voitton(5)</a></li>
                <li><a href="#">Large(90)</a></li>
            </ul> --}}
        </div><!--Shop By-->
        {{-- <div class="aside tag"><!-- Start Tag -->
            <h4 class="heading">Tags</h4>
            <div class="tag-main">
                <a href="#" class="button tag">Shop</a>
                <a href="#" class="button tag">Jeen</a>
                <a href="#" class="button tag">Jacket</a>
                <a href="#" class="button tag">Fashion</a>
                <a href="#" class="button tag">Men Store</a>
                <a href="#" class="button tag">Women</a>
                <a href="#" class="button tag">Footwear</a>
                <a href="#" class="button tag">Dress</a>
            </div>
        </div><!-- End Tags --> --}}
        <div class="aside compare"><!-- Start Compare -->
            <h4 class="heading">Compare</h4>
            <div class="compare-main">
                <div class="owl-carousel compare-slide">
                    @foreach (App\Models\Product::limit(5)->get() as $compare)
                        <a href="#"><img src="{{ asset('uploads/productImage/'.$compare->image) }}" alt=""/></a>
                    @endforeach
                </div>
            </div>
        </div><!-- End Compare -->
    </div><!--End Side Bar-->
