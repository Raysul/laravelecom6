@extends('layouts.app')
@section('title')
    wifibd:Home
@endsection
@section('content')

@php
    foreach ($data as $key => $value) {
        $$key = $value;
    }
@endphp


     <!--------------Slider and categories-------------->
     <div class="wrapper-slider clearfix">
        <div class="spacer30"></div><!--spacer-->
        <div class="container">
            <div class="row">
                <!---------Start categories----------->
                <div class="col-md-3 category">
                    <div class="category-contant">
                        <h4 class="heading">Categories<i class="pull-right fa fa-angle-down"></i></h4>
                        <a id="categories" class="mobile-categories" href="#"><span class="all_categories">Categories</span><i class="pull-right fa fa-angle-right"></i></a>
                        <ul id="category-menu">
                            <li>
                                <a href="#">Electronics<i class="pull-right fa fa-angle-right"></i></a>

                            </li>
                            @foreach ($sub_categoriess as $sub_category)
                                <li>
                                    <a href="#">{{ $sub_category->name }}<i class="pull-right fa fa-angle-right"></i></a>
                                    @if(App\Models\Category::where('child_id',$sub_category->id)->count() > 0)
                                        <div class="side-sub-menu"><!--Tow column-->
                                            <ul class="menu-col-1 clearfix">
                                                <li class="col-md-12">
                                                    <div class="row">
                                                        <ul class="clearfix col-md-6">
                                                            @foreach (App\Models\Category::where('child_id',$sub_category->id)->get() as $child_category)
                                                                <li><h5>{{ $child_category->name }}</h5></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!---------End category----------->
                <!---------Slider block----------->
                <div class="col-md-6 col-sm-8 slider-main">
                    <div>
                        <div class="b_silde clearfix">
                            <!---------Start slider----------->
                            <div class="owl-carousel slider">
                                @foreach ($sliders as $slider)
                                    <div class="item">
                                        <a href="#"><img src="{{ asset('uploads/sliderImage/'.$slider->image) }}" alt="img-1"/></a>
                                        <div class="caption">
                                            <h3><span>{{ $slider->name }}</span></h3>
                                            <span>{{ $slider->title }}</span>
                                            {{-- <a href="#">Order Now</a> --}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!---------End slider----------->
                            <!--------------Deal product slider---------------------->
                            <div class="deal-product">
                                <h4 class="heading">Best sale of the week</h4>
                                <div class="owl-carousel week-deal item-block">
                                    @foreach ($best_sale_of_the_weeks as $best_sale_of_the_week)
                                        <div class="product-item">
                                            <ul class="products-row">
                                                <li class="image-block">
                                                    <span class="new offer">New</span>
                                                <a href="{{route('product.details',$best_sale_of_the_week->id)}}"><span><img src="{{ asset('uploads/productImage/'.$best_sale_of_the_week->image) }}" alt=""/></span></a>

                                                        <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $best_sale_of_the_week->id }})">Add to cart</a>
                                                    <div class="a-link">
                                                        <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                        <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $best_sale_of_the_week->id }})"><i class="fa fa-heart"></i></a>

                                                        <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                        <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                    </div>
                                                    <div class="review_star">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half"></i>
                                                    </div>
                                                </li>
                                                <li class="products-details">
                                                    <a href="#">
                                                        {{$best_sale_of_the_week->name}}
                                                    </a>
                                                <span>{{$best_sale_of_the_week->price}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!--End deal product slider-->
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="s-right-block clearfix">
                        <div class="right-banner">
                            <a href="#">
                                <img src="{{ asset('frontend') }}/images/banner/banner-img-6.jpg" alt=""/>
                                <span class="banner-details">Get 10% Off</span>
                            </a>
                        </div>
                        <div class="customer-choice"><!-- Choice Product-->
                            <h4 class="heading">Customer's Choice</h4>
                            @foreach($customer_choices as $customer_choice)
                            <div class="choice-product clearfix">
                                <a href="{{route('product.details',$customer_choice->id)}}">
                                    <div class="choice-images">
                                        <a href="{{route('product.details',$customer_choice->id)}}"><img src="{{ asset('uploads/productImage/'.$customer_choice->image) }}" alt=""/></a>
                                    </div>
                                    <div class="choice-text">
                                    <h5>{{$customer_choice->name}}</h5>
                                        <span>{{'$'.$customer_choice->price}}</span>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------------End slider and categories-------------->
     <div class="spacer30"></div><!--spacer-->
    <!--------------Start wrapper main-------------->
    <div class="wrapper-main">
        <!-------------- Start banner block -------------->
        <div class="banner-block clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#">
                            <img src="{{ asset('frontend') }}/images/banner/banner-img-3.jpg" alt=""/>
                            <div class="banner-contant b-right">
                                <div class="banner-off">Off</div>
                                <span class="b-big">30%</span>
                                <ul>
                                    <li>
                                        <span class="b-name">Casual Dress</span>
                                    </li>
                                    <li>
                                        <span class="b-xs-big">&amp; Materials</span>
                                    </li>
                                </ul>
                                <span class="b-btn">Shop Now</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!-- Spacer -->
        </div>
        <!-------------- End banner -------------->
        <!--------------Womens products-------------->
        <div class="product-wrapper clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="product clearfix">
                            <h4 class="heading">
                                <span>New Products</span>
                            </h4>
                            <div class="product-category">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li	class="active">
                                        <h4 class="heading">
                                            <a class="tab-arrow" role="tab" data-toggle="tab" href="#women-product-0">Woman Style</a>
                                        </h4>
                                    </li>
                                    <li>
                                        <a class="small menu-icons clothing" href="#women-product-1" role="tab" data-toggle="tab"><i class="fa fa-shirtsinbulk"></i>Clothing</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons footwear" href="#women-product-2" role="tab" data-toggle="tab"><i class="fa fa-paw"></i>Footwear</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons jewellery" href="#women-product-3" role="tab" data-toggle="tab"><i class="fa fa-diamond"></i>Jewellery</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="product-contant clearfix">
                                <div class="product-banner">
                                    <a href="#">
                                        <img src="{{ asset('frontend') }}/images/banner/women-img-1.jpg" alt=""/>
                                    </a>
                                    <div class="banner-link">
                                        <a href="#">
                                            <i class="fa fa-shopping-bag"></i>
                                            Shop Now
                                        </a>
                                    </div>
                                </div>
                                <div class="product-block clearfix">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="women-product-0">
                                            <div class="item-block clearfix">
                                                @foreach ($woman_styles as $woman_style)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <span class="new offer">New</span>
                                                            <a href="{{route('product.details',$woman_style->id)}}"><span><img src="{{ asset('uploads/productImage/'.$woman_style->image) }}" alt=""/></span></a>
                                                            <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $woman_style->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $woman_style->id }})"><i class="fa fa-heart"></i></a>

                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{$woman_style->name}}
                                                            </a>
                                                            <span>{{'$'.$woman_style->price}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endforeach

                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="women-product-1">
                                            <div class="item-block clearfix">
                                            @foreach ($woman_clothings as $woman_clothing)
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="{{route('product.details',$woman_clothing->id)}}"><span><img src="{{ asset('uploads/productImage/'.$woman_clothing->image) }}" alt=""/></span></a>
                                                        <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $woman_clothing->id }})">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                            <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $woman_clothing->id }})"><i class="fa fa-heart"></i></a>

                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-half"></i>
                                                        </div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            {{ $woman_clothing->name }}
                                                        </a>
                                                        <span>{{'$'. $woman_clothing->price }}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            @endforeach
                                        </div>

                                        </div>
                                        <div class="tab-pane fade" id="women-product-2">
                                            <div class="item-block clearfix">
                                                @foreach ($woman_footwears as $woman_footwear)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$woman_footwear->id)}}"><span><img src="{{ asset('uploads/productImage/'.$woman_footwear->image) }}" alt=""/></span></a>
                                                            <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $woman_footwear->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $woman_footwear->id }})"><i class="fa fa-heart"></i></a>

                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{$woman_footwear->name}}
                                                            </a>
                                                            <span>{{'$'.$woman_footwear->price}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endforeach

                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="women-product-3">
                                            <div class="item-block clearfix">
                                                @foreach ($woman_jewellerys as $woman_jewellery)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$woman_jewellery->id)}}"><span><img src="{{ asset('uploads/productImage/'.$woman_jewellery->image) }}" alt=""/></span></a>
                                                            <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $woman_jewellery->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $woman_jewellery->id }})"><i class="fa fa-heart"></i></a>

                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{$woman_jewellery->name}}
                                                            </a>
                                                            <span>{{'$'.$woman_jewellery->price}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endforeach
                                            </div>

                                        </div>
</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--spacer-->
        </div>
        <!-------------- End womens wroducts -------------->
        <!-------------- Mens products -------------->
        <div class="product-wrapper clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="product clearfix">
                            <h4 class="heading">
                                <span>Featured products</span>
                            </h4>
                            <div class="product-category">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li	class="active">
                                        <h4 class="heading">
                                            <a class="tab-arrow" role="tab" data-toggle="tab" href="#men-product-0">Man Style</a>
                                        </h4>
                                    </li>
                                    <li>
                                        <a class="small menu-icons clothing" href="#men-product-1" role="tab" data-toggle="tab"><i class="fa fa-shirtsinbulk"></i>Clothing</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons footwear" href="#men-product-2" role="tab" data-toggle="tab"><i class="fa fa-paw"></i>Footwear</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons bags" href="#men-product-4" role="tab" data-toggle="tab"><i class="fa fa-briefcase"></i>Bags, Belts & Wallets</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="product-contant clearfix">
                                <div class="product-banner">
                                    <a href="#">
                                        <img src="{{ asset('frontend') }}/images/banner/men-img-1.jpg" alt=""/>
                                    </a>
                                    <div class="banner-link">
                                        <a href="#">
                                            <i class="fa fa-shopping-bag"></i>
                                            Shop Now
                                        </a>
                                    </div>
                                </div>
                                <div class="product-block clearfix">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="men-product-0">
                                            <div class="item-block clearfix">
                                                 @foreach ($man_styles as $man_style)
                                                 <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$man_style->id)}}"><span><img src="{{ asset('frontend') }}/images/products/men/clothing/products-img-6.jpg" alt=""/></span></a>
                                                            <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $man_style->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $man_style->id }})"><i class="fa fa-heart"></i></a>

                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                               {{$man_style->name}}
                                                            </a>
                                                            <span>{{'$'.$man_style->price}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                 @endforeach
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="men-product-1">
                                            <div class="item-block clearfix">
                                                @foreach($man_clothings as $man_clothing)
                                                    <div class="product-item">
                                                        <ul class="products-row">
                                                            <li class="image-block">
                                                                <a href="{{route('product.details',$man_clothing->id)}}"><span><img src="{{ asset('uploads/productImage/'.$man_clothing->image) }}" alt=""/></span></a>
                                                                <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $man_clothing->id }})">Add to cart</a>
                                                                <div class="a-link">
                                                                    <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                    <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $man_clothing->id }})"><i class="fa fa-heart"></i></a>

                                                                    <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                    <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                                </div>
                                                                <div class="review_star">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-half"></i>
                                                                </div>
                                                            </li>
                                                            <li class="products-details">
                                                                <a href="#">
                                                                   {{$man_clothing->name}}
                                                                </a>
                                                                <span>{{'$'.$man_clothing->price}}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endforeach


                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="men-product-2">
                                            <div class="item-block clearfix">
                                                @foreach ($man_footwears as $man_footwear)
                                                    <div class="product-item">
                                                        <ul class="products-row">
                                                            <li class="image-block">
                                                                <a href="{{route('product.details',$man_footwear->id)}}"><span><img src="{{ asset('uploads/productImage/'.$man_footwear->image) }}" alt=""/></span></a>
                                                                <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $man_footwear->id }})">Add to cart</a>
                                                                <div class="a-link">
                                                                    <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                    <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $man_footwear->id }})"><i class="fa fa-heart"></i></a>

                                                                    <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                    <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                                </div>
                                                                <div class="review_star">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-half"></i>
                                                                </div>
                                                            </li>
                                                            <li class="products-details">
                                                                <a href="#">
                                                                    {{$man_footwear->name}}
                                                                </a>
                                                                <span>{{'$'.$man_footwear->price}}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="men-product-4">
                                            <div class="item-block clearfix">
                                                @foreach ($man_Bags as $man_Bag)
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="{{route('product.details',$man_Bag->id)}}"><span><img src="{{ asset('uploads/productImage/'.$man_Bag->image) }}" alt=""/></span></a>
                                                            <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $man_Bag->id }})">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                                <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $man_Bag->id }})"><i class="fa fa-heart"></i></a>

                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                {{$man_Bag->name}}
                                                            </a>
                                                            <span>{{'$'.$man_Bag->price}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endforeach
                                        </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--Spacer-->
        </div>
        <!-------------- End mens products -------------->
        <!-------------- Start banner block -------------->
        <div class="banner-block clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#">
                            <img src="{{ asset('frontend') }}/images/banner/banner-img-4.jpg" alt=""/>
                            <div class="banner-contant b-left">
                                <div class="banner-off">Off</div>
                                <span class="b-big">30%</span>
                                <ul>
                                    <li>
                                        <span class="b-name">All Gents</span>
                                    </li>
                                    <li>
                                        <span class="b-xs-big">Shirts &amp; Jeans</span>
                                    </li>
                                </ul>
                                <span class="b-btn">Shop Now</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!-- spacer -->
        </div>
        <!-------------- End banner -------------->
        <!-------------- Accessories products -------------->
        {{-- <div class="product-wrapper clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="product clearfix">
                            <h4 class="heading">
                                <span>New Arrivals</span>
                            </h4>
                            <div class="product-category">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li	class="active">
                                        <h4 class="heading">
                                            <a class="tab-arrow" role="tab" data-toggle="tab" href="#phone-product-0">Phone & Accessories</a>
                                        </h4>
                                    </li>
                                    <li>
                                        <a class="small menu-icons clothing" href="#phone-product-1" role="tab" data-toggle="tab"><i class="fa fa-shirtsinbulk"></i>Mobiles</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons footwear" href="#phone-product-2" role="tab" data-toggle="tab"><i class="fa fa-paw"></i>Tablets</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons bags" href="#phone-product-4" role="tab" data-toggle="tab"><i class="fa fa-briefcase"></i>Laptops</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons watches" href="#phone-product-5" role="tab" data-toggle="tab"><i class="fa fa-clock-o"></i>Computer Peripherals</a>
                                    </li>
                                    <li>
                                        <a class="small menu-icons sunglasses" href="#phone-product-7" role="tab" data-toggle="tab"><i class="fa fa-envira"></i>Mobile Accessories</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="product-contant clearfix">
                                <div class="product-banner">
                                    <a href="#">
                                        <img src="{{ asset('frontend') }}/images/banner/accesorise-img-1.jpg" alt=""/>
                                    </a>
                                    <div class="banner-link">
                                        <a href="#">
                                            <i class="fa fa-shopping-bag"></i>
                                            Shop Now
                                        </a>
                                    </div>
                                </div>
                                <div class="product-block clearfix">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="phone-product-0">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <span class="new offer">New</span>
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/accessories/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">

                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <span class="pro offer">10% Off</span>
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/computers/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/laptops/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/mobiles/products-img-7.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="phone-product-1">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/mobiles/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/mobiles/products-img-7.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/mobiles/products-img-8.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/mobiles/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="phone-product-2">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/tablets/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/tablets/products-img-7.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/tablets/products-img-8.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/tablets/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="phone-product-4">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/laptops/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/laptops/products-img-7.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/laptops/products-img-8.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/laptops/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="phone-product-5">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/computers/products-img-6.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/computers/products-img-7.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/computers/products-img-8.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/computers/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="phone-product-7">
                                            <div class="item-block clearfix">
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/accessories/products-img-9.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/accessories/products-img-10.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">
                                                                Professional Hxr-Mc2500
                                                                Camcorder Camera
                                                            </a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">

                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/accessories/products-img-11.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="product-item">
                                                    <ul class="products-row">
                                                        <li class="image-block">
                                                            <a href="#"><span><img src="{{ asset('frontend') }}/images/products/phone/accessories/products-img-12.jpg" alt=""/></span></a>
                                                            <a class="add-to-cart" href="#">Add to cart</a>
                                                            <div class="a-link">
                                                                <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                                <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                                <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                                <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                            </div>
                                                            <div class="review_star">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half"></i>
                                                            </div>
                                                        </li>
                                                        <li class="products-details">
                                                            <a href="#">Professional Hxr-Mc2500 Camcorder Camera</a>
                                                            <span>$1600</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="category-block clearfix">
                                                <div class="left-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Footwear
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-1.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="mid-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Sunglasses
                                                            <span>Minimum 20% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-2.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                                <div class="right-discount">
                                                    <a href="#">
                                                        <h4>
                                                            Men Bags
                                                            <span>Minimum 40% off</span>
                                                        </h4>
                                                        <span>
                                                            <img src="{{ asset('frontend') }}/images/products/phone/discount/products-img-3.jpg" alt=""/>
                                                        </span>
                                                        <p>Shop Now</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!-- Spacer -->
        </div> --}}
        <!-------------- End  accessories products -------------->
        <!-------------- Start banner block -------------->
        <!--<div class="banner-block clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#">
                            <img src="{{ asset('frontend') }}/images/banner/banner-img-5.jpg" alt=""/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!-- Spacer
        </div>-->
        <!-------------- End banner -------------->
        <!-------------- Start sellers -------------->
        <div class="sellers clearfix">
            <div class="container">
                <div class="seller-block">
                    <div class="row">
                        <h4 class="heading">Top Sellers In</h4>
                        <div class="col-sm-3">
                            <h5>Mobile</h5>
                            @foreach ($top_sallers_mobailes as $top_sallers_mobaile)
                            <div class="choice-product clearfix">
                                <a href="{{route('product.details',$top_sallers_mobaile->id)}}">
                                    <div class="choice-images">
                                        <img src="{{ asset('uploads/productImage/'.$top_sallers_mobaile->image) }}" alt=""/>
                                    </div>
                                    <div class="choice-text">
                                    <h5>{{ $top_sallers_mobaile->name }}</h5>
                                        <span>{{ '$'.$top_sallers_mobaile->price }}</span>
                                    </div>
                                </a>
                            </div>
                            @endforeach
  </div>
                        <div class="col-sm-3">
                            <h5>Computers</h5>
                            @foreach ($top_sallers_computers as $top_sallers_computer)
                                <div class="choice-product clearfix">
                                    <a href="{{route('product.details',$top_sallers_computer->id)}}">
                                        <div class="choice-images">
                                            <img src="{{ asset('uploads/productImage/'.$top_sallers_computer->image) }}" alt=""/>
                                        </div>
                                        <div class="choice-text">
                                        <h5>{{ $top_sallers_computer->name }}</h5>
                                            <span>{{ '$'.$top_sallers_computer->price }}</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-sm-3">
                            <h5>Clothes</h5>
                            @foreach ($top_sallers_clothess as $top_sallers_clothes)
                                <div class="choice-product clearfix">
                                    <a href="{{route('product.details',$top_sallers_clothes->id)}}">
                                        <div class="choice-images">
                                            <img src="{{ asset('uploads/productImage/'.$top_sallers_clothes->image) }}" alt=""/>
                                        </div>
                                        <div class="choice-text">
                                            <h5>{{ $top_sallers_clothes->name }}</h5>
                                            <span>{{ '$'.$top_sallers_clothes->price }}</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
</div>
                        <div class="col-sm-3">
                            <h5>Accessories</h5>
                            @foreach ($top_sallers_accessoriess as $top_sallers_accessories)
                                <div class="choice-product clearfix">
                                    <a href="{{route('product.details',$top_sallers_accessories->id)}}">
                                        <div class="choice-images">
                                            <img src="{{ asset('uploads/productImage/'.$top_sallers_accessories->image) }}" alt=""/>
                                        </div>
                                        <div class="choice-text">
                                            <h5>{{ $top_sallers_accessories->name }}</h5>
                                            <span>{{ '$'.$top_sallers_accessories->price }}</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-------------- End sellers -------------->
        <div class="spacer30"></div><!-- Spacer -->
        <!-------------- Start Latest Blog -------------->
        <div class="blog-wrapper">
            <div class="container">
                <div class="blog-main">
                <div class="row">
                    <h4 class="heading">Latest-Blog</h4>
                    @foreach ($blogs as $blog)
                    <div class="col-sm-6">
                        <div class="post-item clearfix">
                            <div class="image-block">
                                  <span>
                                      <a href="#"><img src="{{ asset('uploads/blogImage/'.$blog->image) }}" alt=""/></a>
                                   </span>
                               </div>
                              <div class="blog-contant">
                                <div class="post-title">
                                       <h4><a href="#">{{ $blog->title }}</a></h4>
                                   </div>
                                  <div class="post-head">
                                     <ul class="clearfix">
                                         <li><span class="comment"><span>11 Comments Posted by admin </span></span></li>
                                          <li><span class="date"><span>Wednesday, September 24th, 2016</span></span></li>
                                     </ul>
                                  </div>
                                  <div class="min-detail">
                                    <p>{{ $blog->description }}</p>
                                 </div>
                                  <a href="#">Read More</a>
                               </div>
                          </div>
                    </div>
                    @endforeach


                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-------------- End Latest Blog -------------->
        <div class="spacer30"></div><!-- Spacer -->
        <!-------------- Brand wrapper -------------->
        <div class="brand-wrapper">
            <div class="container">
                <div class="brand-main">
                    <div class="owl-carousel brand">
                        <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-1%20-%20Copy.png" alt=""/></a>
                          <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-2%20-%20Copy.png" alt=""/></a>
                          <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-3%20-%20Copy.png" alt=""/></a>
                          <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-4%20-%20Copy.png" alt=""/></a>
                          <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-5%20-%20Copy.png" alt=""/></a>
                          <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-6%20-%20Copy.png" alt=""/></a>
                           <a href="#"><img src="{{ asset('frontend') }}/images/logo/logo-7%20-%20Copy.png" alt=""/></a>
                     </div>
                 </div>
            </div>
        </div>
        <!-------------- Brand wrapper -------------->
@endsection

