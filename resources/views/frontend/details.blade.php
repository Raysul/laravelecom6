@extends('layouts.app')
@section('content')

@php
    foreach ($data as $key => $value) {
        $$key = $value;
    }
@endphp
  <!--XXXXXXXXXX-- Wrapper-breadcrumbs --XXXXXXXXXX-->
  <div class="wrapper-breadcrumbs clearfix">
    <div class="container">
     <div class="spacer30"></div><!--spacer-->
      <div class="breadcrumbs-main clearfix">
      <h2>{{$product->category->name}}</h2>
           <ul>
               <li><a href="#">Home</a><span class="separator">/ </span></li>
             <li><a href="#"> Categories </a><span class="separator">/ </span></li>
             <li><a href="#"> All Categories </a><span class="separator">/ </span></li>
               <li><strong>{{$product->category->name}}</strong></li>
         </ul>
      </div>
     <div class="spacer15"></div><!--spacer-->
   </div>
</div>
<!--XXXXXXXXXX-- End Wrapper-breadcrumbs --XXXXXXXXXX-->
<!--XXXXXXXXXX-- Start Wrapper Main XXXXXXXXXX-->
<div class="wrapper-main brandshop clearfix">
 <div class="spacer15"></div><!--spacer-->
 <div class="container">
     <div class="inner-block"><!------Main Inner-------->
         <div class="row">
             <div class="col-md-9 col-sm-8">
                 <div class="main-contant xs-spacer20 clearfix">
                     <div class="contant-wrapper">
                         <div class="details-view"><!-- Start Product Details -->
                             <div class="clearfix">
                                 <div class="product-img"><!-- Product Images -->
                                     <div id="info-img">
                                         <span class="pro offer">30% off</span>
                                         <div class="swiper-container top-img">
                                             <div class="swiper-wrapper"><!-- Images Slider -->
                                                 <div class="swiper-slide"><img data-zoom-image="{{ asset('uploads/productImage/'.$product->image) }}" src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>

                                                 <div class="swiper-slide"><img data-zoom-image="{{ asset('uploads/productImage/'.$product->image) }}" src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>

                                                 <div class="swiper-slide"><img data-zoom-image="{{ asset('uploads/productImage/'.$product->image) }}" src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>

                                                 <div class="swiper-slide"><img data-zoom-image="{{ asset('uploads/productImage/'.$product->image) }}" src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>
                                             </div>
                                             <!-- Add Arrows -->
                                             <div class="swiper-button-next s-nav fa fa-angle-right"></div>
                                             <div class="swiper-button-prev s-nav fa fa-angle-left"></div>
                                         </div>
                                         <div class="product-thumbs clearfix"><!-- Thumb Images -->
                                             <div data-index="0" class="thumb-item active"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>
                                         <div data-index="1" class="thumb-item"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>
                                             <div data-index="2" class="thumb-item"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>
                                             <div data-index="3" class="thumb-item"><img src="{{ asset('uploads/productImage/'.$product->image) }}" alt="" /></div>
                                         </div>
                                     </div>
                                 </div><!-- End Product Images -->
                                 <div class="product-info">
                                 <h4>{{$product->name}}</h4>
                                     <div class="price-box">
                                         <p class="new-price"><span>{{$product->price}}$</span></p>
                                         <p class="old-price"><span>700$</span></p>
                                     </div>
                                     {{-- {{$product->is_active? <span class="product_stock">Available in stock</span> : 'not_avialble' }} --}}

                                     @if ($product->is_active)
                                     <span class="product_stock">Available in stock</span>
                                     @else
                                     <span class="product_stock">Stock Out</span>
                                     @endif

                                     <div class="short-description">
                                         <p>{{$product->price.'$'}}</p>
                                     </div>
                                     <div class="product_review clearfix">
                                         <div class="review_star">
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star-half"></i>
                                         </div>
                                         <ul class="review_text">
                                             <li><a href="#"><span>(5)</span>Reviews</a></li>
                                             <li><a href="#">Add reviews</a></li>
                                         </ul>
                                     </div>
                                     <div id="attribute" class="attribute clearfix">
                                         <fieldset class="attribute_fieldset">
                                             <label class="attribute_label">Color:</label>
                                             <div class="attribute_list">
                                                 <ul class="attribute_color">
                                                     {{-- <li class="active"><a href="#" class="_1"></a></li> --}}
                                                     @foreach (json_decode($product->color_id) as $color_id)
                                                     @foreach(App\Models\Color::where('id', $color_id)->get() as $color)
                                                     <li style="background:{{ $color->hexadecimal }} ;width: 20px;height: 20px;"><a href="#" class=""></a></li>
                                                     @endforeach
                                                     @endforeach
                                                 </ul>
                                             </div>
                                         </fieldset>

                                         <fieldset class="attribute_fieldset">
                                             <label class="attribute_label">Size:</label>
                                             <div class="attribute_list">
                                                 <ul class="attribute_size">
                                                 {{-- <li class="active"><a href="#">S</a></li> --}}
                                                 @foreach (json_decode($product->size_id) as $size_id)
                                                 @foreach(App\Models\Size::where('id', $size_id)->get() as $size)
                                                 <li><a href="#">{{$size->name}}</a></li>
                                                 @endforeach
                                                 @endforeach
                                                 </ul>
                                             </div>
                                         </fieldset>
                                     </div>
                                     <div class="row">
                                         <div class="col-sm-4 xs-spacer20">
                                             <div class="qty_wrap">
                                                 <label for="prod_qty">Qty:</label><input type="number" min="1" max="50" name="qty" id="prod_qty" class="spinc" value="1" />
                                             </div>
                                         </div>
                                         <div class="cart-btn col-sm-4 col-xs-6">
                                             <a href="#" class="btn">Add to Cart</a>
                                         </div>
                                         <ul class="actions col-sm-4  col-xs-6">
                                             <li><a href="#" class="button fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Reload"></a></li>
                                             <li><a href="#" class="button fa fa-heart selected" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"></a></li>
                                             <li><a href="product-details.html" class="button fa fa-search" data-toggle="tooltip" data-placement="top" title="View Item"></a></li>
                                         </ul>
                                     </div>
                                     <div class="social-links">
                                         <a href="#"><img src="images/i-1.png" alt=""/></a>
                                         <a href="#"><img src="images/i-2.png" alt=""/></a>
                                         <a href="#"><img src="images/i-3.png" alt=""/></a>
                                         <a href="#"><img src="images/i-4.png" alt=""/></a>
                                         <a href="#"><img src="images/i-5.png" alt=""/></a>
                                     </div>
                                 </div>
                             </div>
                         </div><!-- End Product Details -->
                         <div class="spacer30"></div><!--spacer-->
                         <div class="tab-panel clearfix"><!-- Tab -->
                             <!-- Tabs Nav -->
                             <ul class="nav nav-tabs" role="tablist">
                                 <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
                                 <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Comments</a></li>
                                 <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
                             </ul>
                             <!-- Tab panes -->
                             <div class="tab-content">
                                 <div id="description" class="tab-pane active fade in" role="tabpanel">
                                     <div>
                                     <p>{{$product->desc}}</p>

                                     </div>
                                 </div>
                                 <div id="reviews" class="tab-pane fade" role="tabpanel">
                                     <div class="row">
                                         <div class="col-md-6">
                                             <div class="comment-block xs-spacer20">
                                                 <h4>2 Review for “Product Name”</h4>
                                                 <ul>
                                                     <li>
                                                         <img class="user-img" src="images/user_icon.png" alt=""/>
                                                         <div class="meta">
                                                             <strong>Edo</strong>
                                                             <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                         </div>
                                                         <div class="product_review clearfix">
                                                             <div class="review_star">
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star-half"></i>
                                                             </div>
                                                         </div>
                                                         <div class="co-desc">
                                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                         </div>
                                                     </li>
                                                     <li>
                                                         <img class="user-img" src="images/user_icon.png" alt=""/>
                                                         <div class="meta">
                                                             <strong>Edo</strong>
                                                             <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                         </div>
                                                         <div class="product_review clearfix">
                                                             <div class="review_star">
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star"></i>
                                                                 <i class="fa fa-star-half"></i>
                                                             </div>
                                                         </div>
                                                         <div class="co-desc">
                                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                         </div>
                                                     </li>
                                                 </ul>
                                             </div>
                                         </div>
                                         <div class="col-md-6 comment-form">
                                             <div>
                                                 <form action="#">
                                                     <label>Nickname<em>*</em></label>
                                                     <input type="text" class="form-control"/>
                                                     <label>Summary of Your Review<em>*</em></label>
                                                     <input type="text" class="form-control"/>
                                                     <label>Review<em>*</em></label>
                                                     <textarea class="form-control"></textarea>
                                                     <button class="btn" type="submit">Submit Review</button>
                                                 </form>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div id="tags" class="tab-pane fade" role="tabpanel">
                                     <div class="f-categories clearfix">
                                         <div class="col-sm-12">
                                             <ul>
                                                 <li><a href="#">Bag</a></li>
                                                 <li><a href="#">Men's Bags</a></li>
                                                 <li><a href="#">Women's Bags</a></li>
                                                 <li><a href="#">Clothing</a></li>
                                                 <li><a href="#">Dress</a></li>
                                                 <li><a href="#">Pants</a></li>
                                                 <li><a href="#">Shirt</a></li>
                                                 <li><a href="#">T-Shirt</a></li>
                                                 <li><a href="#">Electronics</a></li>
                                                 <li><a href="#">Head phone</a></li>
                                                 <li><a href="#">Phone</a></li>
                                                 <li><a href="#">Laptop</a></li>
                                                 <li><a href="#">electronics</a></li>
                                                 <li><a href="#">Health & Beauty</a></li>
                                             </ul>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="spacer30"></div><!--spacer-->
                         <div class="upsell clearfix">
                             <h4 class="heading">Upsell Products</h4>
                             <div class="owl-carousel upsell-products">
                                @foreach ($top_sallers_mobailes as $top_sallers_mobaile)
                                 <div class="product-item">
                                     <ul class="products-row">
                                         <li class="image-block">
                                             <a href="{{route('product.details',$top_sallers_mobaile->id)}}"><span> <img src="{{ asset('uploads/productImage/'.$top_sallers_mobaile->image) }}" alt=""/></span></a>
                                             <a href="javascript:void(0)" class="add-to-cart" onclick="addToCart({{ $top_sallers_mobaile->id }})">Add to cart</a>
                                                    <div class="a-link">
                                                        <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>

                                                        <a class="l-2" href="javascript:void(0)" onclick="wishlist({{ $top_sallers_mobaile->id }})"><i class="fa fa-heart"></i></a>

                                                        <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                        <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                    </div>
                                                    <div class="review_star">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half"></i>
                                                    </div>
                                                </li>
                                                <li class="products-details">
                                                    <a href="#">
                                                        {{$top_sallers_mobaile->name}}
                                                    </a>
                                                <span>{{$top_sallers_mobaile->price}}</span>
                                                </li>
                                            </ul>
                                 </div>
                                 @endforeach



                             </div>
                         </div>
                     </div>
                 </div>
             </div>
                @include('frontend.partials.sidebar')
            </div>
     </div>
 </div>
 <div class="spacer30"></div><!--spacer-->
 <!--XXXXXXXXXX-- Brand Wrapper --XXXXXXXXXX-->
 <div class="brand-wrapper">
     <div class="container">
         <div class="brand-main">
             <div class="owl-carousel brand">
                 <a href="#"><img src="images/logo/logo-1%20-%20Copy.png" alt=""/></a>
                   <a href="#"><img src="images/logo/logo-2%20-%20Copy.png" alt=""/></a>
                   <a href="#"><img src="images/logo/logo-3%20-%20Copy.png" alt=""/></a>
                   <a href="#"><img src="images/logo/logo-4%20-%20Copy.png" alt=""/></a>
                   <a href="#"><img src="images/logo/logo-5%20-%20Copy.png" alt=""/></a>
                   <a href="#"><img src="images/logo/logo-6%20-%20Copy.png" alt=""/></a>
                    <a href="#"><img src="images/logo/logo-7%20-%20Copy.png" alt=""/></a>
              </div>
          </div>
     </div>
 </div>
 <!--XXXXXXXXXX-- Brand wrapper --XXXXXXXXXX-->
@endsection
